<?php
$productArray = $_SESSION['cart']->get_products();
global $currencies;
$riview->get('loader')->load(array('my-script.php'=> array('type' => 'js')));
?>
<ul id="shopping-cart-popup">
    <?php
    if (count($productArray) > 0) {
        foreach ($productArray as $product) {
            $product_array = explode(':', $product['id']);
            $product_id = $product_array[0];
            ?>
            <li class="product-<?php echo $product_id; ?>">
                <?php echo zen_image('images/'.$product['image'], '', '66', '66'); ?>
                <div class="content-cart">
                <span class="color-45">
                    <?php
                    if((int)strlen($product['name']) > 26)
                        echo substr($product['name'], 0, 26). '...';
                    else
                        echo $product['name'];
                    ?>
                </span>
                    <span>Quantity: <?php echo $product['quantity']; ?></span>
                    <a rel="<?php echo $product_id; ?>" href="javascript:void(0);" class="delete-shopping-cart">
                        <?php echo zen_image(DIR_WS_TEMPLATES . '0007/images/btn_remove.jpg', '', '79', '22'); ?>
                    </a>
                    <?php
                        $price_final = ($product['price'] * $product['quantity']);
                    ?>
                    <span class="price-item-cart"><?php echo $currencies->format($price_final); ?></span>
                </div>
                <div class="clear-both"></div>
            </li>
            <?php
        }
    } else { ?>
        <div class="error-message">
            <?php rie('Your shopping cart is empty!', '', 'riModernZen'); ?>
        </div>
    <?php
    }
    ?>
</ul>
<div class="clear-both"></div>
<div id="total-money-cart">
    <div class="total-price">
        <span class="cart-left color-45"><b><?php rie('Total:', '', 'riModernZen'); ?></b></span>
        <span class="cart-right color-45">
            <?php echo $currencies->format($_SESSION['cart']->show_total()); ?>
        </span>
        <div class="clear-both"></div>
    </div>
    <a id="go-to-shopping-cart" class="btn" href="<?php echo zen_href_link('shopping_cart'); ?>">Shopping Cart</a>
    <a href="<?php echo zen_href_link('xcheckout'); ?>">

        <?php echo zen_image(DIR_WS_TEMPLATES . '0007/images/cart-checkout.jpg', '', '', '', 'id="cart-checkout"'); ?>
    </a>
</div>
