<?php
global $currencies;
$riview->get('loader')->load(array('riElement::frontend/shopping_cart_box/style1/shopping_cart_box.css'));
?>
<div id="header-cart-wrapper" class="dropdown">

    <?php echo zen_image(DIR_WS_TEMPLATE . 'images/cart.png', '', '', '', 'class="shopping-cart-icon"');?>
    <a id="header-cart-link" data-toggle="dropdown" href="#" class="dropdown-toggle link-01" data-target="#">
        <span data-bind="text: totalProduct"></span> <?php rie('itemt(s)');?>
    </a>

    <ul id="shopping-cart-box" class="dropdown-menu" role="menu" aria-labelledby="header-cart-link">

        <!-- ko if: products().length == 0 -->
        <li><?php rie('There is no product in your cart');?></li>
        <!-- /ko -->

        <!-- ko ifnot: products().length == 0 -->
            <!-- ko foreach: products -->
            <li class="product">
                <div class="image">
                    <a class="link" data-bind="attr: { 'href': productsLink }">
                        <span data-bind="html: productsImage"></span>
                    </a>
                </div>
                <div class="name-and-quantity">
                    <div class="name">
                        <a class="link" data-bind="attr: { 'href': productsLink }">
                            <span data-bind="text: name"></span>
                        </a>
                    </div>
                    <div class="quantity">
                        <?php rie('Quantity:');?>
                        <span data-bind="text: quantity"></span>
                    </div>
                </div>

                <div class="price" data-bind="text: display_price"></div>

                <div class="clear-both"></div>
            </li>
            <!-- /ko -->

            <li><?php rie('Total:')?> <span class="total" data-bind="text: total"></span></li>
            <li>
                <a class="btn" href="<?php echo zen_href_link('shopping_cart'); ?>"><?php rie('Shopping Cart');?></a>
                <a class="btn" href="<?php echo zen_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'); ?>">
                    <?php rie('Checkout now');?>
                </a>
            </li>
        <!-- /ko -->
    </ul>

</div>

<?php $riview->get('loader')->load(array('jquery.lib', 'knockout.lib'));?>
<?php $riview->get('loader')->startInline();?>
<script type="text/javascript">

    ko.bindingHandlers.stopBindings = {
        init: function() {
            return { controlsDescendantBindings: true };
        }
    };

    // Class to represent a row in the seat reservations grid
    function Product(properties) {
        var self = this;
        self = jQuery.extend(self, properties);
    }

    // Overall viewmodel for this screen, along with initial state
    function CartViewModel() {
        var self = this;

        // Editable data
        self.products = ko.observableArray([]);
        self.total = ko.observable('');
        self.totalProduct = ko.observable(0);

        // Operations
        self.setProducts = function(products) {
            self.totalProduct(products.length);
            self.products.removeAll();
            jQuery.each(products, function(i,v){
                self.products.push(new Product(v))
            })
        }

//        self.setTotal = function(total){
//            ko.applyBindings({ total: total }, jQuery("#shopping-cart-box .total:first").get(0));
//        }
    }

    var cart_view_model = new CartViewModel();
    ko.applyBindings(cart_view_model, document.getElementById("header-cart-wrapper"));

    <?php
    $products = $_SESSION['cart']->get_products();
    if ($products) {
        foreach ($products as $k => $v) {
            $products_id = zen_get_prid($products[$k]['id']);
            $products[$k]['productsLink'] = zen_href_link(zen_get_info_page($products_id), 'products_id=' . $products_id);
            $products[$k]['display_price'] = $currencies->format($v['price'] * $v['quantity']);
            $products[$k]['productsImage'] = zen_image(DIR_WS_IMAGES . $v['image'], $v['name'], '66', '66');
        }?>
        cart_view_model.setProducts(<?php echo json_encode($products);?>);
    <?php
    }
    ?>

    cart_view_model.total('<?php echo $currencies->format($_SESSION['cart']->show_total()); ?>');
    jQuery(document).on('onCartUpdate', function(e, res){
        cart_view_model.setProducts(res.products)
        cart_view_model.total(res.total);

        var current_link_color = jQuery('#header-cart-link').css('color');
        jQuery('#header-cart-link').stop().animate({color: "#3EB4F6"}, 750)
            .animate({color: current_link_color}, 750);
    });
</script>
<?php $riview->get('loader')->endInline();?>