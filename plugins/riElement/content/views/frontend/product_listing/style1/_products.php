<?php
/**
 * Created by RubikIntegration Team.
 * User: Khanh
 * Date: 8/30/12
 * Time: 4:24 PM
 */

$riview->get('loader')->load(array('riElement::frontend/product_listing/style1/listing.css'));

$page_key = 'page';

//get products
$paginator = plugins\riPlugin\Plugin::get('riResultList.Paginator')
    ->setQuery($listing_sql, 'p.products_id')
    ->setRecordPerPage($record_per_page)
    ->setCurrentPage($_GET[$page_key])
    ->proccess();

$products = plugins\riPlugin\Plugin::get('riProduct.Products')->findBySql($paginator->getRecordsQuery());
?>

<div class="listing clearfix">
    <?php if(!empty($title)):?>
    <div class="title clearfix">
        <h2><?php rie($title, '', 'riModernZen'); ?></h2>
        <div class="bg-line"></div>
    </div>
    <?php endif;?>

    <!-- paginator -->
    <?php
    $paginator_view = $paginator->getTotalPage() > 1 ? $riview->render('riResultList::frontend/style1/_paginator.php', array('paginator' => $paginator,
        'page_key' => $page_key,
        'current_route' => $current_route,
        'record_count' => count($products)
    )) : '';
    echo $paginator_view;
    ?>

    <!-- sorter -->
    <div class="tool-bar">
    <?php
    $sorter_view = !empty($column_list) ? $riview->render('riResultList::frontend/style1/_sorter.php', array('column_list' => $column_list)) : '';
    echo $sorter_view;
    $show_view = $riview->render('riResultList::frontend/style1/_show.php', array('record_per_page' => $record_per_page));
    echo $show_view;
    ?>
    </div>

    <div class="clearfix"></div>

    <?php
    foreach($products as $product){
        echo $riview->render('riElement::frontend/product_listing/style1/_product.php', array('product' => $product));
    }
    ?>

    <div class="clearfix"></div>

    <!-- sorter -->
    <div class="tool-bar">
    <?php
        echo $sorter_view;
        echo $show_view;
    ?>
    </div>

    <!-- paginator -->
    <?php echo $paginator_view; ?>
    <div class="clearfix"></div>
</div>