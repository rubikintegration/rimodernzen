<?php
/**
 * Created by RubikIntegration Team.
 * User: Khanh
 * Date: 8/30/12
 * Time: 4:06 PM
 */
?>
<div class="product">
    <span class="sale"></span>
    <div class="image">
        <a href="<?php echo $product->getLink()?>">
            <?php echo riImage(DIR_WS_IMAGES . $product->productsImage, $product->productsName, '', 'listing');?>
        </a>
    </div>
    <div class="name">
        <a href="<?php echo $product->getLink()?>">
            <?php echo zen_trunc_string($product->productsName, plugins\riPlugin\Plugin::get('settings')->get('theme.listing.products_name_length', 18));?>
        </a>
    </div>
    <div class="price">
        <?php
            global $currencies;
            if($product->getPrice('special') == false){
                echo $currencies->format($product->getPrice('normal'));
            }else{
                echo "<s class='price-normal'>".$currencies->format($product->getPrice('normal'))."</s>&nbsp;";
                echo $currencies->format($product->getPrice('special'));
            }
        ?>
    </div>
    <?php if($product->productIsAlwaysFreeShipping) { ?>
    <p class="free-shipping">
        <?php echo zen_image(DIR_WS_TEMPLATE . 'images/free-shipping.gif', '', '28', '12'); ?>
        <?php rie('FREE SHIPPING', '', 'riModernZen')?>
    </p>
    <?php } ?>
</div>