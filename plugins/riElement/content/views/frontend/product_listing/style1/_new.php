<?php
/**
 * Module Template
 *
 * @package templateSystem
 * @copyright Copyright 2003-2005 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_modules_whats_new.php 2935 2006-02-01 11:12:40Z birdbrain $
 */

use plugins\riPlugin\Plugin;

$riview->get('loader')->load(array('riElement::frontend/product_listing/style1/listing.css'));

?>

<!-- bof: whats_new -->

<div class="listing clearfix" id="new-products">
    <div class="clearfix title">
        <h2><?php rie('New Products', '', 'riModernZen'); ?></h2>
        <div class="bg-line"></div>
    </div>
    <?php
        $conditions = array(sprintf('DATE_ADD( p.products_date_added, INTERVAL %d DAY ) > CURDATE( ) ', $days));
        if(isset($_GET['cPath'])){
            $sub_categories = Plugin::get('riCategory.Tree')->getDeepestLevelChildren($_GET['cPath']);
            $conditions[] = sprintf('p.products_id IN (SELECT distinct products_id FROM ' . TABLE_PRODUCTS_TO_CATEGORIES . ' p2c WHERE p2c.categories_id IN(%s))', implode(',', $sub_categories));
        }

        $products = Plugin::get('riProduct.Products')
            ->findBySql(Plugin::get('riProduct.Products')
            ->generateSql($conditions, $order, $limit));

        foreach($products as $product){
            echo $riview->render('riElement::frontend/product_listing/style1/_product.php', array('product' => $product));
        }
    ?>

    <?php if(!isset($_GET['cPath'])){ ?>
    <div class="view-all">
        <a href="<?php echo zen_href_link('index', 'cPath=1'); ?>"><?php rie('View All New Products >>', '', 'riModernZen'); ?></a>
    </div>
    <?php } ?>

</div>

<!-- eof: whats_new -->
