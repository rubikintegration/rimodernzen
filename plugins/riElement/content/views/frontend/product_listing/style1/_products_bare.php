<?php
/**
 * Created by RubikIntegration Team.
 * Date: 9/14/12
 * Time: 3:16 PM
 * Question? Come to our website at http://rubikintegration.com
 */

$riview->get('loader')->load(array('riElement::frontend/product_listing/style1/listing.css'));
?>
<div class="listing">
    <?php
    // support only the manual handler for now

    foreach($products as $product){
        echo $riview->render('riElement::frontend/product_listing/style1/_product.php', array('product' => $product));
    }
    ?>
</div>