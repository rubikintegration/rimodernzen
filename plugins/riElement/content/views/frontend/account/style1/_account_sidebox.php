<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vunguyen
 * Date: 6/21/12
 * Time: 2:40 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<div id="account-sidebox" class="back">
    <div class="tab active">
        <div class="heading">
            <?php rie('ORDERS');?>
        </div>
        <hr />
        <ul>
            <li>
                <?php echo '<a class="standard" href="' . zen_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL') . '">' . ri('All orders') . '</a>'; ?>
            </li>
        </ul>
    </div>
    <hr />

    <div class="tab">
        <div class="heading">
            <?php rie('ACCOUNT INFO');?>
        </div>
        <hr />
        <ul>
            <li>
                <a class="standard" href="<?php echo zen_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL');?>">
                    My account information
                </a>
            </li>
            <li>
                <a class="standard" href="<?php echo zen_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL');?>">
                    Entries in my address book
                </a>
            </li>
            <li>
                <a class="standard" href="<?php echo zen_href_link(FILENAME_ACCOUNT_PASSWORD, '', 'SSL');?>">
                    Change my account password
                </a>
            </li>

        </ul>
    </div>
</div>
<?php
    $loader->load(array('jquery.lib'));
    $loader->startInline('js');
?>
    <script type="text/javascript">
        jQuery('.tab').mouseenter(function(){
            jQuery('#account-sidebox .tab').removeClass('active');
            jQuery(this).addClass('active');
        });
    </script>
<?php
    $loader->endInline();