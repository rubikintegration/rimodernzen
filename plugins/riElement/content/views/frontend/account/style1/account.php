<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=account.<br />
 * Displays previous orders and options to change various Customer Account settings
 *
 * @package templateSystem
 * @copyright Copyright 2003-2005 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_account_default.php 4086 2006-08-07 02:06:18Z ajeh $
 */

$riview->get('loader')->load('riElement::frontend/account/style1/account.css');
?>

<div class="centerColumn clearfix row" id="accountDefault">

    <?php echo $riview->render('riElement::frontend/account/style1/_account_sidebox.php')?>

    <div id="account-content" class="back">

        <?php if ($messageStack->size('account') > 0) echo $messageStack->output('account'); ?>

        <h2><?php rie('INFORMATION')?></h2>
        <ul class="breadcrumb">
            <li class="active"><?php rie('PREVIOUS ORDERS')?></li>
        </ul>

        <?php
        if (zen_count_customer_orders() > 0) {
            ?>
            <table class="table table-striped" id="prevOrders">
                <thead>
                    <th scope="col"><?php echo TABLE_HEADING_ORDER_NUMBER; ?></th>
                    <th scope="col"><?php echo TABLE_HEADING_DATE; ?></th>
                    <th scope="col"><?php echo TABLE_HEADING_SHIPPED_TO; ?></th>
                    <th scope="col"><?php echo TABLE_HEADING_STATUS; ?></th>
                    <th scope="col"><?php echo TABLE_HEADING_TOTAL; ?></th>
                    <th scope="col"><?php echo TABLE_HEADING_VIEW; ?></th>
                </thead>
                <tbody>
                <?php
                foreach($ordersArray as $orders) {
                    ?>
                    <tr>
                        <td width="30px"><?php echo $orders['orders_id']; ?></td>
                        <td width="70px"><?php echo zen_date_short($orders['date_purchased']); ?></td>
                        <td><address><?php echo zen_output_string_protected($orders['order_name']) . '<br />' . $orders['order_country']; ?></address></td>
                        <td width="70px"><?php echo $orders['orders_status_name']; ?></td>
                        <td width="70px" align="right"><?php echo $orders['order_total']; ?></td>
                        <td align="right"><?php echo '<a class="btn btn-success" href="' . zen_href_link(FILENAME_ACCOUNT_HISTORY_INFO, 'order_id=' . $orders['orders_id'], 'SSL') . '"> ' . ri('View') . '</a>'; ?></td>
                    </tr>

                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
        }
        ?>

        <?php
// only show when there is a GV balance
        if ($customer_has_gv_balance ) {
            ?>
            <div id="sendSpendWrapper">
                <?php require($template->get_template_dir('tpl_modules_send_or_spend.php',DIR_WS_TEMPLATE, $current_page_base,'templates'). '/tpl_modules_send_or_spend.php'); ?>
            </div>
            <?php
        }
        ?>
        <br class="clearBoth" />

    </div>

</div>