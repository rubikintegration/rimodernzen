<?php
/**
 * Created by RubikIntegration Team.
 * Date: 9/18/12
 * Time: 1:17 PM
 * Question? Come to our website at http://rubikintegration.com
 */
?>
<?php $riview->get('loader')->load('riElement::frontend/contact_us/style1/contact_us.css');?>
<div id="contact-wrapper" class="wrapper-978 clearfix">
    <div id="contact-one">
        <div id="intro">
            <h3>Let’s Connect</h3>
            <p>
                <?php require($define_page); ?>
            </p>
        </div>
        <div id="contact-form">
            <h3>Contact Us</h3>
            <div id="form">
                <p>* Required information</p>
                <?php echo zen_draw_form('contact_us', zen_href_link(FILENAME_CONTACT_US, 'action=send')); ?>
                <?php
                if (isset($_GET['action']) && ($_GET['action'] == 'success')) {
                    ?>
                    <div class="mainContent success"><?php echo TEXT_SUCCESS; ?></div>
                    <div class="buttonRow"><a href="<?php echo zen_href_link('index'); ?>" class="btn btn-contact btn-a">Back</a></div>
                    <?php
                } else {
                    ?>
                    <?php if ($messageStack->size('contact') > 0) echo $messageStack->output('contact'); ?>

                    <fieldset id="contactUsForm">
                        <?php
                        if (CONTACT_US_LIST !=''){
                            ?>
                            <label class="inputLabel" for="send-to"><?php echo SEND_TO_TEXT; ?></label>
                            <?php echo zen_draw_pull_down_menu('send_to',  $send_to_array, 0, 'id="send-to"') . '<span class="modern-zen-alert">' . ENTRY_REQUIRED_SYMBOL . '</span>'; ?>
                            <br class="clearBoth" />
                            <?php
                        }
                        ?>

                        <label class="inputLabel" for="contactname"><?php echo ENTRY_NAME; ?></label>
                        <?php echo zen_draw_input_field('contactname', $name, ' size="40" id="contactname"') . '<span class="modern-zen-alert">' . ENTRY_REQUIRED_SYMBOL . '</span>'; ?>
                        <br class="clearBoth" />

                        <label class="inputLabel" for="email-address"><?php echo ENTRY_EMAIL; ?></label>
                        <?php echo zen_draw_input_field('email', ($email_address), ' size="40" id="email-address"') . '<span class="modern-zen-alert">' . ENTRY_REQUIRED_SYMBOL . '</span>'; ?>
                        <br class="clearBoth" />

                        <label class="inputLabel" for="enquiry"><?php echo ENTRY_ENQUIRY ; ?></label>
                        <?php echo zen_draw_textarea_field('enquiry', '30', '7', $enquiry, 'id="enquiry"').'<span class="modern-zen-alert">' . ENTRY_REQUIRED_SYMBOL . '</span>'; ?>

                    </fieldset>

                    <div class="buttonRow forward">
                        <?php //echo zen_image_submit(DIR_WS_TEMPLATE . 'images/send-contact.jpg', BUTTON_SEND_ALT); ?>
                        <a href="<?php echo zen_href_link('index'); ?>" class="btn btn-contact btn-a">Back</a>
                        <input type="submit" class="btn btn-info btn-contact" value="Send">

                    </div>
                    <?php
                }
                ?>
                </form>


            </div>
        </div>
    </div>
    <div id="contact-two">
        <div id="info">
            <h3>Contact Info</h3>
            <p>
                <b>Phone</b>: 123-456-7890<br/>
                <b>Fax</b>: 123-456-7890<br/>
                <b>Email</b>: info@rubikin.com<br/>
                <b>Adress</b>: River Street, 534 4250-123 Porto Portugal
            </p>
        </div>
        <div id="img-map">
            <h3>Map</h3>
            <p>
                <?php echo zen_image(DIR_WS_TEMPLATE . 'images/contact-map.jpg', '', '263', '324'); ?>
            </p>
        </div>
    </div>
</div>