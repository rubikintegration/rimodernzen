<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=account_edit.<br />
 * View or change Customer Account Information
 *
 * @package templateSystem
 * @copyright Copyright 2003-2005 Zen Cart Development Team
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_account_edit_default.php 3848 2006-06-25 20:33:42Z drbyte $
 * @copyright Portions Copyright 2003 osCommerce
 */
$riview->get('loader')->load('riElement::frontend/account/style1/account.css');
?>
<div class="centerColumn clearfix" id="accountEditDefault">
    <?php echo $riview->render('riElement::frontend/account/style1/_account_sidebox.php')?>
    <div id="account-content" class="back">
        <h2><?php echo HEADING_TITLE; ?></h2>
        <?php echo zen_draw_form('account_edit', zen_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL'), 'post', 'onsubmit="return check_form(account_edit);"') . zen_draw_hidden_field('action', 'process'); ?>

        <?php if ($messageStack->size('account_edit') > 0) echo $messageStack->output('account_edit'); ?>

        <fieldset>

        <?php
          if (ACCOUNT_GENDER == 'true') {
        ?>
        <?php echo zen_draw_radio_field('gender', 'm', $male, 'id="gender-male"') . '<label class="radioButtonLabel" for="gender-male">' . MALE . '</label>' . zen_draw_radio_field('gender', 'f', $female, 'id="gender-female"') . '<label class="radioButtonLabel" for="gender-female">' . FEMALE . '</label>' . (zen_not_null(ENTRY_GENDER_TEXT) ? '<span class="alert">' . ENTRY_GENDER_TEXT . '</span>': ''); ?>
        <br class="clearBoth" />
        <?php
          }
        ?>

        <label class="inputLabel" for="firstname"><?php echo ENTRY_FIRST_NAME; ?></label>
        <?php echo zen_draw_input_field('firstname', $account->fields['customers_firstname'], 'id="firstname" class="input-xlarge focused"') ?>
        <br class="clearBoth" />

        <label class="inputLabel" for="lastname"><?php echo ENTRY_LAST_NAME; ?></label>
        <?php echo zen_draw_input_field('lastname', $account->fields['customers_lastname'], 'id="lastname" class="input-xlarge focused"') ?>
        <br class="clearBoth" />

        <?php
          if (ACCOUNT_DOB == 'true') {
        ?>
        <label class="inputLabel" for="dob"><?php echo ENTRY_DATE_OF_BIRTH; ?></label>
        <?php echo zen_draw_input_field('dob', zen_date_short($account->fields['customers_dob']), 'id="dob" class="input-xlarge focused"') ?>
        <br class="clearBoth" />
        <?php
          }
        ?>

        <label class="inputLabel" for="email-address"><?php echo ENTRY_EMAIL_ADDRESS; ?></label>
        <?php echo zen_draw_input_field('email_address', $account->fields['customers_email_address'], 'id="email-address" class="input-xlarge focused"') ?>
        <br class="clearBoth" />

        <label class="inputLabel" for="telephone"><?php echo ENTRY_TELEPHONE_NUMBER; ?></label>
        <?php echo zen_draw_input_field('telephone', $account->fields['customers_telephone'], 'id="telephone" class="input-xlarge focused"') ?>
        <br class="clearBoth" />

        <label class="inputLabel" for="fax"><?php echo ENTRY_FAX_NUMBER; ?></label>
        <?php echo zen_draw_input_field('fax', $account->fields['customers_fax'], 'id="fax" class="input-xlarge focused"') ?>
        <br class="clearBoth" />

        <?php
          if (CUSTOMERS_REFERRAL_STATUS == 2 and $customers_referral == '') {
        ?>
        <label class="inputLabel" for="customers-referral"><?php echo ENTRY_CUSTOMERS_REFERRAL; ?></label>
        <?php echo zen_draw_input_field('customers_referral', '', zen_set_field_length(TABLE_CUSTOMERS, 'customers_referral', 15), 'id="customers-referral" class="input-xlarge focused"'); ?>
        <br class="clearBoth" />
        <?php } ?>

        <?php
          if (CUSTOMERS_REFERRAL_STATUS == 2 and $customers_referral != '') {
        ?>
        <label for="customers-referral-readonly"><?php echo ENTRY_CUSTOMERS_REFERRAL; ?></label>
        <?php echo $customers_referral; zen_draw_hidden_field('customers_referral', $customers_referral,'id="customers-referral-readonly"'); ?>
        <br class="clearBoth" />
        <?php } ?>
        </fieldset>

        <!--
        <fieldset>
        <legend><?php echo ENTRY_EMAIL_PREFERENCE; ?></legend>
        <?php echo zen_draw_radio_field('email_format', 'HTML', $email_pref_html,'id="email-format-html"') . '<label class="radioButtonLabel" for="email-format-html">' . ENTRY_EMAIL_HTML_DISPLAY . '</label>' . zen_draw_radio_field('email_format', 'TEXT', $email_pref_text, 'id="email-format-text"') . '<label  class="radioButtonLabel" for="email-format-text">' . ENTRY_EMAIL_TEXT_DISPLAY . '</label>'; ?>
        <br class="clearBoth" />
        </fieldset>
        -->
        <button class="btn btn-primary"><?php rie('Update')?></button>

        </form>
    </div>
</div>