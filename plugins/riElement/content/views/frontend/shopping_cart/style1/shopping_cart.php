<?php
$riview->get('loader')->load(array('riElement::frontend/shopping_cart/style1/shopping_cart.css'));
global $currencies;
?>
<div id="shopping-cart-wrapper" class="wrapper-978 clearfix">
    <div class="title clearfix">
        <h2>Shopping Cart</h2>
        <div class="bg-line"></div>
    </div><!--end title-->
    <div id="content">
        <dl id='top'>
            <dt>Product</dt>
            <dt>Price</dt>
            <dt>Quantity</dt>
            <dt>Delete</dt>
            <dt>Total</dt>
        </dl><!--end top-->

        <div id='center'>
            <?php
            if(count($productArray)>0):

                    ?>
                    <!-- ko foreach: products -->
                    <div class="items clearfix">
                        <dl data-bind="attr: { 'data-id': id }">
                            <dt>
                                <div class="image">
                                    <a class="link" data-bind="attr: { 'href': productsLink }">
                                        <span data-bind="html: productsImage"></span>
                                    </a>
                                </div>
                                <div class="name-and-attr">
                                    <a class="link" data-bind="attr: { 'href': productsLink }">
                                        <span data-bind="text: name"></span>
                                    </a>
                                    <span data-bind="html: attributeHiddenField"></span>
                                    <ul data-bind="foreach: attributes">
                                        <li><span data-bind="text: products_options_name"></span><?php echo TEXT_OPTION_DIVIDER;?><span data-bind="text: products_options_values_name"></span></li>
                                    </ul>
                                    <input class="product-id" type="hidden" name="productId[]"/>
                                </div>
                            </dt>
                            <dt class="vertical" data-bind="text: productsPriceEach"></dt>
                            <dt class="vertical" data-bind="html: quantityField"></dt>
                            <dt class="vertical">
                                <a class="delete" href="#" data-bind="click: $parent.delete">
                                    <i class="icon-remove"></i>
                                </a>
                            </dt>
                            <dt class="vertical" data-bind="text: productsPrice"></dt>
                        </dl>
                    </div><!--end items-->
                    <!-- /ko -->
                    <?php
            else:
                ?>
                <div class="error-message"> Your shopping cart is empty! </div>
                <?php endif; ?>
        </div> <!--end center-->
        <div id='bottom'>
            <!--div id="sub-total" class="clearfix">
                <span>Subtotal:</span>
                <span class="price-right">$122</span>
            </div>
            <div id="shipping" class="clearfix">
                <span>Shipping:</span>
                <span class="price-right">$4</span>
            </div-->
            <div id="total" class="clearfix">
                <span>Total:</span>
                <span id="total-shopping-cart" class="price-right" data-bind="text: total"></span>
            </div>
            <div id="button">
                <a href="javascript:void(0);" id="shopping-cart-update" class="btn">Update</a>
                <a href="<?php echo zen_href_link('index'); ?>" class="btn">Continue Shopping</a>
                <a href="<?php echo zen_href_link(FILENAME_CHECKOUT_SHIPPING); ?>" class="btn btn-info btn-checkout">CHECKOUT</a>
            </div>
        </div><!--end bottom-->
    </div><!--end content-->
</div><!--end shopping-cart-wrapper-->
<script type="text/javascript">


    //update shopping cart
    jQuery('#shopping-cart-update').on('click',function(){
        //get id
        var products_id = [];
        jQuery('.product-id').each(function() {
            products_id.push(jQuery(this).attr('rel'));
        });
        //get qty
        var qty = [];
        jQuery('input[name="cart_quantity[]"]').each(function(){
            qty.push(jQuery(this).val());
        });
        if(products_id.length>0){
            var strParams = 'totalParams='+products_id.length;
            for(var i=0; i<products_id.length; i++){
                strParams = strParams.concat('&param'+i+'='+products_id[i]+'|'+qty[i]);
            }
            jQuery.ajax({
                url: "<?php //echo riLink('ricart_ajax_edit_cart', array(), $request_type, false, 'ri.php');?>",
                dataType: 'json',
                type: 'post',
                data: strParams,
                success: function(response){
                    window.location.href = '<?php echo zen_href_link(FILENAME_SHOPPING_CART);?>';
                },
                error:function(){
                    alert('Error');
                }
            });
        }
    });

    //delete shopping cart
    jQuery("#shopping-cart-wrapper .delete").on('click', function(){
        var id = jQuery(this).closest('dl').data('id');
        console.log(id);
        return false;
        <!--        jQuery.ajax({-->
        <!--            url: "--><?php //echo riLink('ricart_ajax_remove_from_cart', array(), $request_type, false, 'ri.php');?><!--",-->
        <!--            dataType: 'json',-->
        <!--            type: 'post',-->
        <!--            data: 'id='+id,-->
        <!--            success: function(res){-->
        <!--                jQuery(document).trigger('onCartUpdate', res);-->
        <!--            },-->
        <!--            error:function(){-->
        <!--                alert('Error');-->
        <!--            }-->
        <!--        });-->
    });
    //end delete shopping cart
</script>

<?php $riview->get('loader')->load(array('jquery.lib', 'knockout.lib'));?>
<?php $riview->get('loader')->startInline();?>
<script type="text/javascript">

    // Overall viewmodel for this screen, along with initial state
    function CartPageViewModel() {
        var self = this;

        // Editable data
        self.products = ko.observableArray([]);
        self.total = ko.observable('');
        self.totalProduct = ko.observable(0);

        // Operations
        self.setProducts = function(products) {
            self.totalProduct(products.length);
            self.products.removeAll();
            jQuery.each(products, function(i,v){
                self.products.push(new Product(v))
            })
        }

        self.delete = function(e){
            self.products.remove(e);
        }

    }

    var cart_page_view_model = new CartPageViewModel();
    ko.applyBindings(cart_page_view_model, document.getElementById("shopping-cart-wrapper"));

    <?php
    if (count($productArray) > 0) {
        foreach ($productArray as $k => $v) {
            $products_id = zen_get_prid(zen_get_prid($products[$k]['id']));
            $products[$k] = array_merge($products[$k], $v);
            $products[$k]['productsLink'] = zen_href_link(zen_get_info_page($products_id), 'products_id=' . $products_id);
            $products[$k]['display_price'] = $currencies->format($v['price'] * $v['quantity']);
            $products[$k]['productsImage'] = zen_image(DIR_WS_IMAGES . $products[$k]['image'], $products[$k]['name'], '66', '66');
        }?>
    cart_page_view_model.setProducts(<?php echo json_encode($products);?>);
        <?php
    }
    ?>
</script>
<?php $riview->get('loader')->endInline();?>