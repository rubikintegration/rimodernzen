<?php
/**
 * Created by RubikIntegration Team.
 * Date: 9/4/12
 * Time: 8:44 AM
 * Question? Come to our website at http://rubikintegration.com
 */

$products_id = (int)$_GET['products_id'];
$product = plugins\riPlugin\Plugin::get('riProduct.Products')->findById($products_id);
//how to buy
$sql_ezpages = "select pages_html_text from " . TABLE_EZPAGES . " where pages_id = 1";
$ezpages = plugins\riPlugin\Plugin::get('riProduct.Products')->findBySql($sql_ezpages);

$riview->get('loader')->load('riElement::frontend/product_info/style1/product_info.css');
?>


<div id="product-info" class="info">

    <div class="column-left">
        <div class="main-image">
            <?php echo riImage(DIR_WS_IMAGES.$products_image, $products_name,'','info');?>
        </div>
        <div class="main-image-bottom">
            <a href="<?php echo DIR_WS_IMAGES.$products_image; ?>" class="large-img">
                <span class="zoom-img"></span>
                <?php rie('Larger images', '', 'riModernZen'); ?>
            </a>
        </div>

        <?php
        $additional_images = riGetProductAdditionalImages($products_image);
        if(count($additional_images) > 0):?>
            <div class="additional-image">


            </div>
            <?php endif;?>

        <div class="product-action">
            <span id="tell"></span><p id="tell-label"><a href="#" class="link-share-info"><?php rie('Tell a Friend' ,'', 'riModernZen'); ?></a></p>
            <span id="write"></span><p id="write-label"><a href="#" class="link-share-info"><?php rie('Write a Reviews', '', 'riModernZen'); ?></a></p>
            <span id="read"></span><p id="read-label"><a href="#" class="link-share-info"><?php rie('Read the Reviews', '', 'riModernZen'); ?> </a></p>
        </div>
    </div><!--end div product-info-right-img-->

    <div class="column-right">
        <h1 class="name"><?php echo $products_name; ?></h1>
        <?php if(zen_get_product_is_always_free_shipping($products_id) && $flag_show_product_info_free_shipping) { ?>
        <p class="free-shipping">
            <?php echo zen_image(DIR_WS_TEMPLATE . 'images/free-shipping.gif', '', '28', '12'); ?>
            <?php rie('FREE SHIPPING', '', 'riModernZen'); ?>
        </p>
        <?php } ?>

        <?php
        if($product->getPrice('special') == false){?>
            <p class="price">
                <span class="label-left-01"><?php rie('Price:', '', 'riModernZen') ; ?></span><span class="color-red-dark"><?php echo $currencies->format($product->getPrice('normal'));; ?></span>
            </p>
            <?php }else{ ?>
            <div class="price list">
                <span id="label-left"><?php rie('List price:', '', 'riModernZen'); ?></span><s><?php echo $currencies->format($product->getPrice('normal')); ?></s>
            </div>

            <p class="price">
                <span class="label-left-01"><?php rie('Price:', '', 'riModernZen'); ?></span><span class="color-red-dark"><?php echo $currencies->format($product->getPrice('special')); ?></span>
            </p>
            <?php } ?>

        <!--form process add cart -->
        <form class="form-option-qty" enctype="multipart/form-data" method="post" action="javascript:void(0);" name="cart_quantity">
            <div id="product-wrapper">
                <?php
                if(count($options_name)>0){
                    ?>
                    <p id="please-choose">
                        <?php rie('Please Chose:', '', 'riModernZen'); ?>
                    </p>
                    <?php
                    for($i=0;$i<sizeof($options_name);$i++) {
                        ?>
                        <p class="product-info-right-price">
                            <span class="left-info-option"><?php echo $options_name[$i]; ?></span>
                            <?php echo $options_menu[$i]; ?>
                        </p>
                        <?php }
                }
                ?>
            </div>
            <div id="product-cart">
                <span>Qty</span>
                <input type="text" class="qty" value="1" name="cart_quantity">

                <button class="btn btn-cart"><?php rie('Add to Cart')?></button>

                <?php echo "<a id='wishlist_add_link' href='".str_replace("%3Aproducts_id", $products_id, $wishlish_link_href)."' class='$wishlish_link_class btn btn-wishlist'><i class='icon-star' ></i>Add to Wishlist</a>"; ?>
                <input type="hidden" value="<?php echo $products_id; ?>" name="products_id">
            </div>
        </form><!--end form process add cart -->

        <!-- holder: productInfoBox1 -->

        <div class="clear-both"></div>

    </div><!--end div product-info-right-img-->
    <div class="clear-both"></div>
    <div id="product-tabs">
        <ul>
            <li id="product-tabs-description" class="product-info-active" rel="info">
                <?php rie('Products Detail', '', 'riModernZen'); ?><span class="current-span"></span>
            </li>
            <li id="product-tabs-additional" rel="usage">
                <?php rie('Shipping & Returns', '', 'riModernZen'); ?><span></span>
            </li>
        </ul>
        <div id="product-tabs-description-content">
            <div id="info">
                <?php echo $products_description; ?>
            </div>
            <div id="usage">
                <?php echo $ezpages[0]['pages_html_text']; ?>
            </div>
        </div>

        <!-- holder: productInfoBox2 -->

    </div><!--end div product-info-right-intro-->

    <div class="clear-both"></div>
</div>
<script type="text/javascript">
    //jquery lightbox
    jQuery(document).ready(function(){
        jQuery(function() {
            //jQuery('#product-info-right-bottom a').lightBox();
        });

        //process product info display
        jQuery('#product-tabs ul li').live('click', function(){
            var rel = jQuery(this).attr('rel');
            jQuery('#product-tabs ul li').removeClass('product-info-active');
            jQuery('#product-tabs ul li span').removeClass('current-span');

            jQuery(this).addClass('product-info-active');
            jQuery(this).children().addClass('current-span');

            jQuery('#product-tabs-description-content').children().hide();
            jQuery('#product-tabs-description-content #'+rel).show();

        });
    })
</script>