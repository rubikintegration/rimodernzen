<?php
/**
 * Created by RubikIntegration Team.
 * Date: 8/27/12
 * Time: 10:11 PM
 * Question? Come to our website at http://rubikintegration.com
 */
?>
<ul class="ul-level-<?php echo $level?>">
<?php
    foreach($menus_tree[$menus_id]['sub_menus'] as $menu) {?>
        <li class="li-level-<?php echo $level?>">
            <a class="a-level-<?php echo $level?> <?php if($menus_tree[$menu]['is_active']) echo 'active' ?>" href="<?php echo $menus_tree[$menu]['menus_link']?>">
                <?php echo $menus_tree[$menu]['menus_name'];?>
            </a>
            <?php
                if($menus_tree[$menu]['has_children']&& $level < 1){?>
                    <?php echo $riview->render('riMenu::frontend/style11/_level_1.php', array('menus_tree' => $menus_tree, 'menus_id' => $menu, 'level' => $level+1));
                }
            ?>
        </li>
<?php }?>
</ul>