<?php
    $riview->get('loader')->load('riElement::frontend/menu/style11/menus.css');
?>

<?php
use plugins\riPlugin\Plugin;
$menus_tree = Plugin::get('riMenu.Tree')->getTree();
$link_current = Plugin::get('riUtility.Uri')->getCurrent();
Plugin::get('riMenu.Tree')->checkActive($menus_tree, $menus_id, $link_current);
?>
<div id="nav-wrapper">
    <div class="wrapper-978 clearfix">
<?php
echo $riview->render('riMenu::frontend/style11/_level_1.php', array('menus_tree' => $menus_tree, 'menus_id' => $menus_id, 'level' => 0));

?>
    </div>
</div>
<?php
/*
function menu($menu_tree,$sub_menu,&$dem, $found_active = false){
        $link_current = Plugin::get('riUtility.Uri')->getCurrent();
        global $this_is_home_page;
        $i = count($sub_menu);
        $main_page = $_GET['main_page'];
        
       
        foreach($sub_menu as $item){
            
            --$i;
            $tam = $dem;
            if($menu_tree[$item]['has_children'] == 1 ){
                
                echo "<li class='li-level-".$dem."'>".$menu_tree[$item]['menus_name']."<ul class='ul-level-".($dem+1)."'>";
                menu($menu_tree,$menu_tree[$item]['sub_menus'],++$tam, $found_active);
            }
            else{
                $class_cur = "";
                $link = zen_href_link($menu_tree[$item]['menus_main_page'], $menu_tree[$item]['menus_parameters']);
                $_link = str_replace("&amp;", "&", $link);
                    
                if(!$found_active){
                    if($this_is_home_page){
                        $class_cur = "active";
                        $found_active = true;
                    }else if(strpos($_link, $link_current) !== false){
                        $class_cur = "active";
                        $found_active = true;
                    }
                }
                echo "<li class='li-level-link-".$dem."'><a class='" . $class_cur  . " a-level-".$dem."' href='".$link."'>".$menu_tree[$item]['menus_name']."</a></li>";
            }
           if($i == 0){
               if($dem != 0)
                    echo "</ul></li>";
               else
                   echo "</ul>";
           }
            
        }

}
$dem = 0;
*/
?>

<?php $riview->get("loader")->load(array('jquery.lib'));
$riview->get("loader")->startInline('js');
?>
<script type="text/javascript">
    jQuery("#nav-wrapper a.active").parentsUntil("#menu-top", ".li-level-0").addClass(" current-menu-top-li");
    //hover menu top
    jQuery('#nav-wrapper .li-level-0').mouseenter(function(){
        jQuery(this).find('.ul-level-1').show();
    }).mouseleave(function(){
        jQuery(this).find('.ul-level-1').hide();
    });
</script>    

<?php $riview->get("loader")->endInline();?>