<?php
/**
 * Created by RubikIntegration Team.
 * Date: 9/3/12
 * Time: 9:09 AM
 * Question? Come to our website at http://rubikintegration.com
 */

$riview->get('loader')->load(array('riElement::frontend/header/style1/header.css'));

?>
<div id="logo-warraper">
    <div class="wrapper-978">
        <a href="<?php echo zen_href_link('index');?>" class="no-underline">
            <?php echo zen_image(DIR_WS_TEMPLATE . 'images/logo-rubik.jpg', '', '285', '67', 'id="logo"');?>
        </a>
    </div>

</div><!--end header-->
<div class="clear-both"></div>
<div id="header-bar-1">
    <div class="wrapper-978">
        <div id="menu-left">
            <?php echo zen_draw_form('advanced_search', zen_href_link(FILENAME_ADVANCED_SEARCH_RESULT), 'get', 'id="search-mini-form"') . zen_hide_session_id(); ?>
            <?php echo zen_draw_hidden_field('main_page', FILENAME_ADVANCED_SEARCH_RESULT); ?>
            <input type="text" name="keyword" id="search" placeholder="<?php rie('Enter search keyword', '', 'riModernZen')?>"/>
            <?php echo zen_image_submit('search.gif', '', 'name="btn-search" id="btn-search"'); ?>
            </form>
        </div><!--end menu-left-->
        <div id="menu-right">

            <?php if(!$_SESSION['customer_id']){ ?>
                <a href="<?php echo zen_href_link('xcheckout'); ?>" class="link-01">
                    <?php rie('Register', '', 'riModernZen')?>
                </a>
                <span class="line-vertical">|</span>
                <a href="<?php echo zen_href_link('xcheckout'); ?>" class="link-01">
                    <?php rie('Login', '', 'riModernZen')?>
                </a>
            <?php }else{?>
                <a href="<?php echo zen_href_link('account'); ?>" class="link-01">
                    <?php rie('My Account', '', 'riModernZen')?>
                </a>
                <span class="line-vertical">|</span>
                <a href="<?php echo zen_href_link('logoff'); ?>" class="link-01">
                    <?php rie('Logout', '', 'riModernZen')?>
                </a>
            <?php } ?>
            <span class="line-vertical">|</span>
            <?php echo $riview->render('riElement::frontend/shopping_cart_box/style1/_shopping_cart_box.php');?>
            <!--box hover cart-->
            <div id="box-hover-cart">

            </div>
            <!--end box cart-->

        </div><!--end menu-right-->
    </div>
</div><!--end menu-->

<!--menu top -->
<!-- holder: globalBox2 -->
<!--end menu top -->