<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=account_history.<br />
 * Displays all customers previous orders
 *
 * @package templateSystem
 * @copyright Copyright 2003-2005 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_account_history_default.php 2580 2005-12-16 07:31:21Z birdbrain $
 */
$riview->get('loader')->load('riElement::frontend/account/style1/account.css');
?>
<div class="centerColumn clearfix" id="accountHistoryDefault">

    <?php echo $riview->render('riElement::frontend/account/style1/_account_sidebox.php')?>
    <div id="account-content" class="back">
        <h2 id="accountHistoryDefaultHeading"><?php echo HEADING_TITLE; ?></h2>
            <?php
        if ($accountHasHistory === true) {?>
            <table class="table table-stripped">
                <thead>
                    <th>
                        <?php rie('Order Id')?>
                    </th>
                    <th>
                        <?php rie('Status')?>
                    </th>
                    <th>
                        <?php rie('Date Purchased')?>
                    </th>
                    <th>
                        <?php rie('Info')?>
                    </th>
                    <th>
                        <?php rie('Total')?>
                    </th>
                    <th>
                        <?php rie('View')?>
                    </th>
                </thead>
                <tbody>
                <?php foreach ($accountHistory as $history) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $history['orders_id']; ?>
                        </td>
                        <td>
                            <?php echo $history['orders_status_name'];?>
                        </td>
                        <td>
                            <?php echo zen_date_long($history['date_purchased']);?>
                        </td>
                        <td>
                            <?php echo '<strong>' . $history['order_type'] . '</strong> ' . zen_output_string_protected($history['order_name']);?>
                        </td>
                        <td>
                            <?php echo strip_tags($history['order_total'])?>
                        </td>
                        <td>
                            <?php echo '<a class="btn btn-success" href="' . zen_href_link(FILENAME_ACCOUNT_HISTORY_INFO, (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'order_id=' . $history['orders_id'], 'SSL') . '">' . ri('View') . '</a>'; ?>
                        </td>

                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <div class="navSplitPagesLinks forward"><?php echo TEXT_RESULT_PAGE . ' ' . $history_split->display_links(MAX_DISPLAY_PAGE_LINKS, zen_get_all_get_params(array('page', 'info', 'x', 'y', 'main_page'))); ?></div>
            <div class="navSplitPagesResult"><?php echo $history_split->display_count(TEXT_DISPLAY_NUMBER_OF_ORDERS); ?></div>
            <?php
        } else {
            ?>
            <div class="centerColumn" id="noAcctHistoryDefault">
                <?php echo TEXT_NO_PURCHASES; ?>
            </div>
            <?php
        }
        ?>

    </div>
</div>