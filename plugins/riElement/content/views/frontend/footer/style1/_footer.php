<?php
/**
 * Created by RubikIntegration Team.
 * Date: 9/3/12
 * Time: 9:46 AM
 * Question? Come to our website at http://rubikintegration.com
 */

$riview->get('loader')->load(array('riElement::frontend/footer/style1/footer.css'));

?>
<div id="footer-top" class="wrapper-978">
        <div class="footer-top-01">

            <!-- holder: globalBox4 -->

        </div>
        <div class="footer-top-01">
            <ul>
                <h4><?php rie('Customer Care', '', 'riModernZen'); ?></h4>
                <li><a href="<?php echo zen_href_link('contact_us'); ?>"><?php rie('Contact Us', '', 'riModernZen'); ?></a></li>
                <li><a href="<?php echo zen_href_link('help'); ?>"><?php rie('Help', '', 'riModernZen'); ?></a></li>
                <li><a href="<?php echo zen_href_link('store'); ?>"><?php rie('Store Locator', '', 'riModernZen'); ?></a></li>
                <li><a href="#"><?php rie('In-Store Pickup', '', 'riModernZen'); ?></a></li>
                <li><a href="#"><?php rie('In-Store Services', '', 'riModernZen'); ?></a></li>
            </ul>
        </div>
        <div class="footer-top-01">
            <ul>
                <h4><?php rie('Information', '', 'riModernZen'); ?></h4>
                <li><a href="<?php echo zen_href_link('about'); ?>"><?php rie('About Us', '', 'riModernZen'); ?></a></li>
                <li><a href="<?php echo zen_href_link('press'); ?>"><?php rie('Press', '', 'riModernZen'); ?></a></li>
                <li><a href="<?php echo zen_href_link('investor'); ?>"><?php rie('Investor Relations', '', 'riModernZen'); ?></a></li>
                <li><a href="<?php echo zen_href_link('privacy'); ?>"><?php rie('Privacy Policy', '', 'riModernZen'); ?></a></li>
            </ul>
        </div>
        <div class="footer-top-02">
            <ul>
                <h4><?php rie('We Accept', '', 'riModernZen'); ?></h4>
                <li>
                    <?php echo zen_image(DIR_WS_TEMPLATE.'images/visa.gif', '', '', '', 'class="icon-bank"');?>
                    <?php echo zen_image(DIR_WS_TEMPLATE.'images/master.gif', '', '', '', 'class="icon-bank"');?>
                    <?php echo zen_image(DIR_WS_TEMPLATE.'images/amex.gif', '', '', '', 'class="icon-bank"');?>
                    <?php echo zen_image(DIR_WS_TEMPLATE.'images/visa2.gif', '', '', '', 'class="icon-bank"');?>
                <li>
                <li class="same-first-child"><?php rie('Newsletter', '', 'riModernZen'); ?></li>
                <li><?php rie('Sign Up for Our Newsletter:', '', 'riModernZen'); ?></li>
                <li>
                    <input type="text" name="newsletter" id="newsletter"/>
                    <button name="btn_newsletter" class="btn btn-ri" id="btn_newsletter">Subscribe</button>
                </li>
            </ul>
        </div>
</div>
<div id="footer-bottom">
    <p id="footer-copy-right" class="wrapper-978">
        <?php rie('Copyright &copy;2009 %store_name%. Developed by <a class="link-color-blue no-underline" href="http://rubikintegration.com">Rubik Integration</a>. All rights reserved.', array('%store_name%' => STORE_NAME), 'riModernZen');?>
    </p>
</div>