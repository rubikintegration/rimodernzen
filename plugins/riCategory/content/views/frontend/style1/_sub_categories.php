<?php
/**
 * Created by RubikIntegration Team.
 * Date: 9/2/12
 * Time: 5:14 PM
 * Question? Come to our website at http://rubikintegration.com
 */
$riview->get('loader')->load(array('riCategory::frontend/style1/listing.css'));

?>
<div class="listing clearfix">
    <?php
    while(!$categories->EOF){
        ?>
        <div class="category-box">
            <div class="image">
                <a href="<?php echo zen_href_link('index', 'cPath=' . $categories->fields['categories_id']);?>">
                    <?php echo riImage(DIR_WS_IMAGES . $categories->fields['categories_image'], $categories->fields['categories_name'], '', 'listing');?>
                </a>
            </div>
            <div class="name">
                <a href="<?php echo zen_href_link('index', 'cPath=' . $categories->fields['categories_id']);?>">
                    <?php echo $categories->fields['categories_name'];?>
                </a>
            </div>
        </div>
        <?php
        $categories->MoveNext();
    }
    ?>
</div>