<?php
$riview->get('loader')->load('riBanner::style1/banners.css');
?>
<div class="banners" id="<?php echo $slider_id;?>">
<?php
use plugins\riPlugin\Plugin;
$banners = Plugin::get('riBanner.Banners')->get($group, $limit, $order);
if(count($banners) > 0){ ?>
     <div class="coin-slider">
         <?php foreach($banners as $banner): ?>
            <a href="<?php echo $banner['banners_url'];?>">
                <?php echo zen_image('images/'.$banner['banners_image'], '', '', '', 'class="banner-image"');?>
		 <span>
			The best <span style="color: #090909;">alternatives</span> to READ
		 </span>
            </a>
         <?php endforeach; ?>
     </div>
<?php
}
?>
</div>
<?php $riview->get('loader')->load(array('jquery.lib', 'jquery.coinSlider.lib'));?>
<?php $riview->get('loader')->startInline('js')?>

<?php
    $default_slider_options = array('navigation' => true, 'delay' => 3000, 'hoverPause' => true);
    $custom_slider_options = !isset($slider_options) ? $default_slider_options : array_merge($default_slider_options, $slider_options);
?>
<script type="text/javascript">
    jQuery('#<?php echo $slider_id;?>').coinslider(<?php echo json_encode($custom_slider_options);?>);
</script>

<?php $riview->get('loader')->endInline('js')?>