<?php
use plugins\riPlugin\Plugin;
$menus_tree = Plugin::get('riMenu.Tree')->getTree();
?>
<ul>
    <h4>Categories</h4>
     <?php
    foreach($menus_tree[$menus_id]['sub_menus'] as $menu):
    ?>
            <li>
                <a class="link-menu-footer" href="<?php echo zen_href_link($menus_tree[$menu]['menus_main_page'],$menus_tree[$menu]['menus_parameters']); ?>">
                    <?php echo $menus_tree[$menu]['menus_name'] ; ?>
                </a>
            </li>
    <?php
    endforeach;
    ?>
</ul>