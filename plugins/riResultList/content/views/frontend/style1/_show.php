<?php
/**
 * Created by RubikIntegration Team.
 * Date: 9/2/12
 * Time: 1:26 PM
 * Question? Come to our website at http://rubikintegration.com
 */
?>
<form class="show-form inactivated" method="get" action="<?php echo zen_href_link('index');?>">
    <?php foreach(riGetAllGetParams(array('record_per_page')) as $name => $value):?>
        <input type="hidden" name="<?php echo $name;?>" value="<?php echo $value;?>" />
    <?php endforeach;?>
    <input type="hidden" name="record_per_page" value="<?php echo $_GET['record_per_page'];?>" />
    <?php rie('Show:', '', 'riModernZen'); ?>
    <div class="bg-dropbox" id="bg-dropbox-02">
        &nbsp;<?php echo $record_per_page;?> &nbsp;
        <?php echo zen_image(DIR_WS_TEMPLATE . 'images/listing_08.png'); ?>
        <div class="dropbox-number">
            <ul>
                <?php for ($k = 5; $k <= 30; $k+=5) { ?>
                <li>
                    <a class="filter-link-sort" href="#" data-target="input[name=record_per_page]" data-value="<?php echo $k; ?>">
                        <?php echo $k; ?>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</form>

<?php $riview->get('loader')->load('jquery.lib')?>
<?php $riview->get('loader')->startInline('js')?>
<script type="text/javascript">
    jQuery('.show-form.inactivated').each(function(){
        jQuery(this).find('a').on('click', function(e){
            var form = jQuery(this).closest('form');
            form.find(jQuery(this).data('target')).val(jQuery(this).data('value'));
            form.submit();
            e.preventDefault();
        });

        jQuery(this).find('.bg-dropbox').on('click', function(){
            jQuery(this).find('.dropbox-number').toggle();
            jQuery(this).find('.dropbox-number-01').hide();
        });

        jQuery(this).removeClass('inactivated');

    });
</script>
<?php $riview->get('loader')->endInline()?>