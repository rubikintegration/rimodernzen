<form class="sorter-form inactivated" method="get" action="<?php echo zen_href_link('index');?>">
    <?php foreach(riGetAllGetParams(array('sort', 'record_per_page')) as $name => $value):?>
    <input type="hidden" name="<?php echo $name;?>" value="<?php echo $value;?>" />
    <?php endforeach;?>
    <input type="hidden" name="sort" value="<?php echo $_GET['sort'];?>" />
    <input type="hidden" name="record_per_page" value="<?php echo $_GET['record_per_page'];?>" />

    <?php rie('Sort by:', '', 'riModernZen'); ?>
    <div class="bg-dropbox">
            &nbsp;<?php rie('Default', '', 'riModernZen'); ?> &nbsp;
            <?php echo zen_image(DIR_WS_TEMPLATE . 'images/listing_08.png'); ?>
            <div class="dropbox-number-01">
                <ul>
                    <?php foreach($column_list as $index => $column):?>
                    <li>
                        <?php
                        $text = '';
                        switch($column){
                            case 'PRODUCT_LIST_MODEL':
                                $text = "Product Model";
                                break;
                            case 'PRODUCT_LIST_NAME':
                                $text = "Product Name";
                                break;
                            case 'PRODUCT_LIST_MANUFACTURER':
                                $text = "Product Manufacturer";
                                break;
                            case 'PRODUCT_LIST_QUANTITY':
                                $text = "Product Quantity";
                                break;
                            case 'PRODUCT_LIST_WEIGHT':
                                $text = "Product Weight";
                                break;
                            case 'PRODUCT_LIST_PRICE':
                                $text = "Product Price";
                                break;
                        }
                        ?>

                        <?php if(!empty($text)):?>
                            <a class="filter-link-sort" href="#" data-target="input[name=sort]" data-value="<?php echo $index+1?>a">
                                <?php rie($text, '', 'riModernZen'); ?>
                            </a>
                        <?php endif;?>

                    </li>
                    <?php endforeach;?>
                </ul>
            </div>
    </div>
</form>

<?php $riview->get('loader')->load('jquery.lib')?>
<?php $riview->get('loader')->startInline('js')?>
<script type="text/javascript">
    jQuery('.sorter-form.inactivated').each(function(){
        jQuery(this).find('a').on('click', function(e){
            var form = jQuery(this).closest('form');
            form.find(jQuery(this).data('target')).val(jQuery(this).data('value'));
            form.submit();
            e.preventDefault();
        });

        jQuery(this).find('.bg-dropbox').on('click', function(){
            jQuery(this).find('.dropbox-number-01').toggle();
            jQuery(this).find('.dropbox-number').hide();
        });

        jQuery(this).removeClass('inactivated');

    })
</script>
<?php $riview->get('loader')->endInline()?>