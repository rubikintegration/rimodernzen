<?php
/**
 * Created by RubikIntegration Team.
 * User: Khanh
 * Date: 8/30/12
 * Time: 4:12 PM
 */

$current_page = $paginator->getCurrentPage();
$total_page = $paginator->getTotalPage();
if(!isset($page_distance)) $page_distance = 5;
$loader->load('riResultList::frontend/style1/pagination.css');
?>

<ul class="pagination">
<?php if($total_page > 1): ?>
    <?php if($current_page > 1): ?>
            <li class="page">
                <a class="prev-page" href="<?php echo zen_href_link($current_route, http_build_query(riGetAllGetParams(array($page_key, 'main_page'), array($page_key => $current_page-1))));?>"></a>
            </li>
    <?php endif; ?>
    <?php if($current_page - $page_distance > 1):?>
        <li class="page">
            <a href="<?php echo zen_href_link($current_route, http_build_query(riGetAllGetParams(array($page_key, 'main_page'), array($page_key => 1))));?>">1</a>
        </li>
        <?php if($current_page - $page_distance > 2):?>
            <li class="page">
                ...
            </li>
        <?php endif;?>
    <?php endif;?>

    <?php for($page = $current_page - $page_distance; $page < $current_page; $page++):?>
        <?php if($page > 0):?>
            <li class="page"><a
                href="<?php echo zen_href_link($current_route, http_build_query(riGetAllGetParams(array($page_key, 'main_page'), array($page_key => $page))));?>"><?php echo $page;?>
            </a>
            </li>
        <?php endif;?>
    <?php endfor;?>
    <li class="page current"><a
        href="<?php echo zen_href_link($current_route, http_build_query(riGetAllGetParams(array($page_key, 'main_page'), array($page_key => $current_page))));?>"><?php echo $current_page;?>
    </a>
    </li>
    <?php for($page = $current_page+1; $page < $current_page + $page_distance && $page <= $total_page; $page++):?>
        <li class="page">
            <a href="<?php echo zen_href_link($current_route, http_build_query(riGetAllGetParams(array($page_key, 'main_page'), array($page_key => $page))));?>"><?php echo $page;?></a>
        </li>
    <?php endfor;?>

    <?php if($page < $total_page):?>
        <?php if($total_page - $current_page > $page_distance):?>
            <li class="page">
                ...
            </li>
        <?php endif;?>
        <li class="page">
            <a href="<?php echo zen_href_link($current_route, http_build_query(riGetAllGetParams(array($page_key, 'main_page'), array($page_key => $total_page))));?>"><?php echo $total_page;?></a>
        </li>
    <?php endif;?>
    <?php if($current_page < $total_page): ?>
            <li class="page">
                <a class="next-page" href="<?php echo zen_href_link($current_route, http_build_query(riGetAllGetParams(array($page_key, 'main_page'), array($page_key => $current_page+1))));?>"></a>
            </li>
    <?php endif; ?>
<?php endif; ?>
</ul>
<?php //rie('Displaying %record_count% of %total_record% results', array('%record_count%' => $record_count, '%total_record%' => $paginator->getTotalRecord()));