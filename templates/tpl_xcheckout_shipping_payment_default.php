<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=checkout_shipping.<br />
 * Displays allowed shipping modules for selection by customer.
 *
 * @package templateSystem
 * @copyright Copyright 2003-2007 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_checkout_shipping_default.php 6964 2007-09-09 14:22:44Z ajeh $
 */
?>
<?php if(\plugins\riPlugin\Plugin::get('riCheckout.Ajax')->startBlock('xcheckout_step')){?>
<div id="checkoutShipping">

<?php echo zen_draw_form('checkout_payment', zen_href_link(FILENAME_XCHECKOUT_CONFIRMATION, '', 'SSL'), 'post', ($flagOnSubmit ? '' : '')); ?>
<?php echo zen_draw_hidden_field('action', 'process');?>
<?php //if ($messageStack->size('checkout_shipping') > 0) echo $messageStack->output('checkout_shipping'); ?>

<?php
  if (DISPLAY_CONDITIONS_ON_CHECKOUT == 'true') {
?>
<fieldset>
<legend><span class="termsconditions"><?php rie('Terms and Conditions'); ?></span></legend>
<div><span class="termsdescription"><?php rie('Please acknowledge the terms and conditions bound to this order by ticking the following box. The terms and conditions can be read <a href="%link%"><span class="pseudolink">here</span></a>', array('%link%' => zen_href_link(FILENAME_CONDITIONS, '', 'SSL')), 'riCheckout');?></div>
<?php echo  zen_draw_checkbox_field('conditions', '1', false, 'id="conditions"');?>
<label class="checkboxLabel" for="conditions"><span class="termsiagree"><?php rie('I have read and agreed to the terms and conditions bound to this order.')?></span></label>
</fieldset>
<?php
  }
?>

<div id="xcheckout_shipping_container" class="xcheckout-left">
    <?php if($_SESSION['sendto'] !== false) :?>
<h2 class="title_header"><?php rie('DELIVERY METHOD');?></h2>
<div class="clear-both"></div>
<?php
  if (zen_count_shipping_modules() > 0) {
?>

<?php
    if (sizeof($quotes) > 1 && sizeof($quotes[0]) > 1) {
?>

	<div id="checkoutShippingContentChoose" class="important title-text"><?php //echo TEXT_CHOOSE_SHIPPING_METHOD; ?></div>

<?php
    } elseif ($_SESSION['free_shipping'] == false) {
?>
	<div id="checkoutShippingContentChoose" class="important title-text"><?php //echo TEXT_ENTER_SHIPPING_INFORMATION; ?></div>

<?php
    }
?>
<?php
    if ($_SESSION['free_shipping'] == true) {
?>
<div id="freeShip" class="important" ><?php echo FREE_SHIPPING_TITLE; ?>&nbsp;<?php echo $quotes[$i]['icon']; ?></div>
<div id="defaultSelected"><?php echo sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format(MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER)) . zen_draw_hidden_field('shipping', 'free_free'); ?></div>

<?php
    } else {
      $radio_buttons = 0;
      for ($i=0, $n=sizeof($quotes); $i<$n; $i++) {
      // bof: field set
// allows FedEx to work comment comment out Standard and Uncomment FedEx
      if ($quotes[$i]['id'] != '' || $quotes[$i]['module'] != '') { // FedEx
 //     if ($quotes[$i]['module'] != '') { // Standard
?>
<!--<fieldset>
<legend><?php echo $quotes[$i]['module']; ?>&nbsp;<?php if (isset($quotes[$i]['icon']) && zen_not_null($quotes[$i]['icon'])) { echo $quotes[$i]['icon']; } ?></legend>-->

<?php
        if (isset($quotes[$i]['error'])) {
?>
      <div><?php echo $quotes[$i]['error']; ?></div>
<?php
        } else {
          for ($j=0, $n2=sizeof($quotes[$i]['methods']); $j<$n2; $j++) {
// set the radio button to be checked if it is the method chosen
            $checked = (($quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'] == $_SESSION['shipping']['id']) ? true : false);

            if ( ($checked == true) || ($n == 1 && $n2 == 1) ) {
              //echo '      <div id="defaultSelected" class="moduleRowSelected">' . "\n";
            //} else {
              //echo '      <div class="moduleRow">' . "\n";
            }
?>
		<?php echo zen_draw_radio_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'], $checked, 'id="ship-'.$quotes[$i]['id'] . '-' . $quotes[$i]['methods'][$j]['id'].'" style="float:left"'); ?>
		<label style="float:left;" for="ship-<?php echo $quotes[$i]['id'] . '-' . $quotes[$i]['methods'][$j]['id']; ?>" class="checkboxLabel" ><?php echo $quotes[$i]['methods'][$j]['title']; ?></label>

<?php
            if ( ($n > 1) || ($n2 > 1) ) {
?>
<div class="important back"><?php echo '(' . $currencies->format(zen_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))) . ')'; ?></div>
<?php
            } else {
?>
<div class="important back"><?php echo '(' . $currencies->format(zen_add_tax($quotes[$i]['methods'][$j]['cost'], $quotes[$i]['tax'])) . zen_draw_hidden_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id']) . ')'; ?></div>
<?php
            }
?>

<!--</div>-->
<br class="clear-both" />
<?php
            $radio_buttons++;
          }
        }
?>
<?php
//</fieldset>

    }
// eof: field set
      }
    }
?>

<?php
  } else {
?>
	<h2 id="checkoutShippingHeadingMethod"><?php echo TITLE_NO_SHIPPING_AVAILABLE; ?></h2>
	<div id="checkoutShippingContentChoose" class="important"><?php echo TEXT_NO_SHIPPING_AVAILABLE; ?></div>
<?php
  }
?>


<!--
<div class="buttonRow back">
<?php echo zen_draw_checkbox_field('set_xpress', 1, false, 'id="set_xpress"') ?>
<label class="inputLabel2" for="set_xpress"><?php echo TEXT_EXPRESS_SHIPPING; ?> (<a class="tooltip" title="<?php echo TEXT_EXPRESS_SHIPPING_METHOD_HELP;?>" href="#">?</a>)</label>
</div>
-->
	

		<div class="clear-both"></div>
    <?php endif;?>

    <?php // ** BEGIN PAYPAL EXPRESS CHECKOUT **
    if (!$payment_modules->in_special_checkout()) {
        // ** END PAYPAL EXPRESS CHECKOUT ** ?>

        <?php // ** BEGIN PAYPAL EXPRESS CHECKOUT **
    }
    // ** END PAYPAL EXPRESS CHECKOUT ** ?>

    <?php
    $selection =  $order_total_modules->credit_selection();
    if (sizeof($selection)>0) {
        for ($i=0, $n=sizeof($selection); $i<$n; $i++) {
            if ($_GET['credit_class_error_code'] == $selection[$i]['id']) {
                ?>
                <div class="messageStackError"><?php echo zen_output_string_protected($_GET['credit_class_error']); ?></div>

                <?php
            }
            for ($j=0, $n2=sizeof($selection[$i]['fields']); $j<$n2; $j++) {
                ?>
                <?php if(!($COWOA && $selection[$i]['module']==MODULE_ORDER_TOTAL_GV_TITLE)) {?>
                    <div class="clear-both"></div>

                    <h2 class="title_header"><?php echo $selection[$i]['module'];?></h2>
                    <div class="clear-both"></div>
                    <label><?php echo $selection[$i]['redeem_instructions']; ?></label>
                    <div class="gvBal larger"><?php echo $selection[$i]['checkbox']; ?></div>
                    <label class="inputLabel"<?php echo ($selection[$i]['fields'][$j]['tag']) ? ' for="'.$selection[$i]['fields'][$j]['tag'].'"': ''; ?>><?php //echo $selection[$i]['fields'][$j]['title']; ?></label>
                    <br>
                    <div class="clear-both"></div>
                    <?php echo $selection[$i]['fields'][$j]['field']; ?>

                    <?php } ?>
                <?php
            }
        }
        ?>
        <div class="buttonRow forward">
            <?php //echo xcheckout_button('button_xcheckout_apply.gif', 'xcheckout', TEXT_NEW_ADDRESS, 'class="toggle_elements"');?>
        </div>
        <?php
    }
    ?>

    <fieldset class="shipping" id="comments">
        <legend><?php rie('Special Instructions or Order Comments'); ?></legend>
        <?php echo zen_draw_textarea_field('comments', '40', '2'); ?>
    </fieldset>
</div>


<!-- begin payments -->
<div id="xcheckout_payment_container" class="xcheckout-left">
<h2 class="title_header"><?php rie('Payment Method', null, 'riCheckout');?></h2>
<div class="clear-both"></div>
	<?php // ** BEGIN PAYPAL EXPRESS CHECKOUT **
	if (!$payment_modules->in_special_checkout()) {
			  // ** END PAYPAL EXPRESS CHECKOUT ** ?>

			<?php
			  if (SHOW_ACCEPTED_CREDIT_CARDS != '0') {
			?>

			<?php
				if (SHOW_ACCEPTED_CREDIT_CARDS == '1') {
				  echo TEXT_ACCEPTED_CREDIT_CARDS . zen_get_cc_enabled();
				}
				if (SHOW_ACCEPTED_CREDIT_CARDS == '2') {
				  echo TEXT_ACCEPTED_CREDIT_CARDS . zen_get_cc_enabled('IMAGE_');
				}
			?>
			<br class="clear-both" />
			<?php } ?>

			<?php
			  foreach($payment_modules->modules as $pm_code => $pm) {
				if(substr($pm, 0, strrpos($pm, '.')) == 'googlecheckout') {
				  unset($payment_modules->modules[$pm_code]);
				}
			  }
			  $selection = $payment_modules->selection();

			  if (sizeof($selection) > 1) {
			?>
			<p class="important title-text"><?php rie('Please select a payment method', null, 'riCheckout') ?></p>
			<?php
			  } elseif (sizeof($selection) == 0) {
			?>
			<!--<p class="important"><?php rie('Please select a payment method', null, 'riCheckout'); ?></p>-->

			<?php
			  }
			?>

			<?php
			  $radio_buttons = 0;$n=sizeof($selection);
			  for ($i= $n-1; $i>=0; $i--) {
			?>
            <div class="pmt-method">
            <div class="pmt-selection">
			<?php
				if (sizeof($selection) > 1) {
					if (empty($selection[$i]['noradio'])) {
			 ?>
			<?php echo zen_draw_radio_field('payment', $selection[$i]['id'], ($selection[$i]['id'] == $_SESSION['payment'] ? true : false), 'id="pmt-'.$selection[$i]['id'].'"'); ?>
			<?php   } ?>
			<?php
				} else {
					
			?>
			<?php echo zen_draw_hidden_field('payment', $selection[$i]['id']); ?>
			<?php
				}
			?>
            </div>
			<label for="pmt-<?php echo $selection[$i]['id']; ?>" class="radioButtonLabel"><?php echo $selection[$i]['module']; ?></label>

			<?php
				if (defined('MODULE_ORDER_TOTAL_COD_STATUS') && MODULE_ORDER_TOTAL_COD_STATUS == 'true' and $selection[$i]['id'] == 'cod') {
			?>
			<div class="alert"><?php echo TEXT_INFO_COD_FEES; ?></div>
			<?php
				} else {
				  // echo 'WRONG ' . $selection[$i]['id'];
			?>
			<?php
				}
			?>
			<br class="clear-both" />

			<?php
				if (isset($selection[$i]['error'])) {
			?>
				<div><?php echo $selection[$i]['error']; ?></div>

			<?php
				} elseif (isset($selection[$i]['fields']) && is_array($selection[$i]['fields'])) {
			?>

			<div class="ccinfo">
			<?php
				  for ($j=0, $n2=sizeof($selection[$i]['fields']); $j<$n2; $j++) {
				    
			?>
			<label 
			<?php 
			      echo (isset($selection[$i]['fields'][$j]['tag']) ? 'for="'.$selection[$i]['fields'][$j]['tag'] . '" ' : ''); 
			?>class="inputLabelPayment">
			<?php echo $selection[$i]['fields'][$j]['title']; ?></label>
			<?php echo $selection[$i]['fields'][$j]['field']; ?>
			<div class="clear-both"></div>
			<?php
				  }
				 
			?>
			</div>
			<div class="clear-both"></div>
			<?php
			  
				}
				$radio_buttons++;
			?>
			<div class="clear-both"></div>
            </div>
			<?php
			  }
			?>

<?php // ** BEGIN PAYPAL EXPRESS CHECKOUT **
      } else {
        ?><input type="hidden" name="payment" value="<?php echo $_SESSION['payment']; ?>" /><?php
      }
      // ** END PAYPAL EXPRESS CHECKOUT ** ?>


</div>

	<div class="clear-both"></div>
	<span class="loader" style="display: none;">
    <?php rie('Loading');?>
    </span>
    <button class="xcheckout-next-button btn btn-primary xcheckout-right btn-x"><?php rie('Continue')?></button>
    <div class="clear-both"></div>
	</form>
</div>
<?php \plugins\riPlugin\Plugin::get('riCheckout.Ajax')->endBlock();}
require($template->get_template_dir('tpl_modules_xcheckout_tracking.php', DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout'). '/' . 'tpl_modules_xcheckout_tracking.php');