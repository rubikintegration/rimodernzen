<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=checkout_success.<br />
 * Displays confirmation details after order has been successfully processed.
 *
 * @package templateSystem
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_checkout_success_default.php 5407 2006-12-27 01:35:37Z drbyte $
 */
?>
<?php if(\plugins\riPlugin\Plugin::get('riCheckout.Ajax')->startBlock('xcheckout_step')){?>
<div id="checkoutSuccess">

<!--bof -gift certificate- send or spend box-->
<?php
// only show when there is a GV balance
  if ($customer_has_gv_balance ) {
?>
<div id="sendSpendWrapper">
<?php require($template->get_template_dir('tpl_modules_send_or_spend.php',DIR_WS_TEMPLATE, $current_page_base,'templates'). '/tpl_modules_send_or_spend.php'); ?>
</div>
<?php
  }
?>
<!--eof -gift certificate- send or spend box-->

<div id="checkoutSuccessOrderNumber"><?php rie('<strong>Your Order Number is:</strong>', null, 'riCheckout'); ?> <?php echo $zv_orders_id; ?></div>
<?php if (DEFINE_CHECKOUT_SUCCESS_STATUS >= 1 and DEFINE_CHECKOUT_SUCCESS_STATUS <= 2) { ?>
<div id="checkoutSuccessMainContent" class="content">
<?php
/**
 * require the html_defined text for checkout success
 */
  require($define_page);
?>
</div>
<?php } ?>
<!--bof logoff-->
<div id="checkoutSuccessLogoff">
<?php
  if (isset($_SESSION['customer_guest_id'])) {
    rie('NOTE: To complete your order, a temporary account was created. You may close this account by clicking Log Off. Clicking Log Off also ensures that your receipt and purchase information is not visible to the next person using this computer. If you wish to continue shopping, feel free! You may log off at anytime using the link at the top of the page.', null, 'riCheckout');
  } elseif (isset($_SESSION['customer_id'])) {
    rie('Thank you for shopping. Please click the Log Off link to ensure that your receipt and purchase information is not visible to the next person using this computer.', null, 'riCheckout');
  }
?>
    <br/>
<a class="btn" href="<?php echo zen_href_link(FILENAME_LOGOFF, '', 'SSL'); ?>">Logout</a>
</div>
<!--eof logoff-->
<br class="clear-both" />
<!--bof -product notifications box-->
<?php
/**
 * The following creates a list of checkboxes for the customer to select if they wish to be included in product-notification
 * announcements related to products they've just purchased.
 **/
    if ($flag_show_products_notification == true && !($_SESSION['COWOA'])) {
?>
<fieldset id="csNotifications">
<legend><?php rie('Please notify me of updates to these products', null, 'riCheckout'); ?></legend>
<?php echo zen_draw_form('order', zen_href_link(FILENAME_XCHECKOUT_SUCCESS, 'action=update', 'SSL'), 'class="not_ajax"'); ?>

<?php foreach ($notificationsArray as $notifications) { ?>
<?php echo zen_draw_checkbox_field('notify[]', $notifications['products_id'], true, 'id="notify-' . $notifications['counter'] . '"') ;?>
<label class="checkboxLabel" for="<?php echo 'notify-' . $notifications['counter']; ?>"><?php echo $notifications['products_name']; ?></label>
<br />
<?php } ?>
<?php echo xcheckout_submit('', 'xcheckout', BUTTON_UPDATE_ALT, 'class="btn"'); ?>
</form>
</fieldset>
<?php
    }
?>
<!--eof -product notifications box-->

<!--bof -product downloads module-->
<?php
  if (DOWNLOAD_ENABLED == 'true') require($template->get_template_dir('tpl_modules_downloads.php',DIR_WS_TEMPLATE, $current_page_base,'templates'). '/tpl_modules_downloads.php');
?>
<!--eof -product downloads module-->

<?php if(!($_SESSION['COWOA'])) { ?> <div id="checkoutSuccessOrderLink">
    <?php rie('You can view your order history by going to the <a href="' . zen_href_link(FILENAME_ACCOUNT, '', 'SSL') . '" name="linkMyAccount">My Account</a> page and by clicking on "View All Orders".', null, 'riCheckout');?>
</div> <?php } ?>

<div id="checkoutSuccessContactLink">
    <?php rie('Please direct any questions you have to <a href="' . zen_href_link(FILENAME_CONTACT_US) . '" name="linkContactUs">customer service</a>.', null, 'riCheckout'); ?>
</div>

<h3 id="checkoutSuccessThanks" class="centeredContent"><?php rie('Thanks for shopping with us online!', null, 'riCheckout'); ?></h3>
</div>
<?php \plugins\riPlugin\Plugin::get('riCheckout.Ajax')->endBlock();}
require($template->get_template_dir('tpl_modules_xcheckout_tracking.php', DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout'). '/' . 'tpl_modules_xcheckout_tracking.php');