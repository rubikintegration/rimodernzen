<?php
/**
* @package page template
* @copyright Copyright 2003-2006 Zen Cart Development Team
* @copyright Portions Copyright 2003 osCommerce
* @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
* @version $Id: Define Generator v0.1 $
*/

// THIS FILE IS SAFE TO EDIT! This is the template page for your new page 

$riview->get('loader')->load(array('riCheckout::checkout.css', 'jquery.lib', 'jquery.form.lib', 'jquery.includeMany.lib', 'jquery.validation.lib', 'jquery.wtooltip.lib', 'xcheckout.lib', 'riCheckout::checkout.php' => array('type' => 'js')))
?>
<noscript><?php rie("Please enable Javascript on your browser to checkout! You can also opt for the classic checkout out by click <a href='%link%'>here</a>", array('%link%' => zen_href_link('checkout_shipping', 'xcheckout_bypass=1')), 'riCheckout'); ?></noscript>

<div class="centerColumn">
<div id="xcheckout">

<?php

ob_start();

$tabindex = 1;
$load_step = true;
foreach($steps as $step => $info){
	$class ="";
	if($load_step)
		if($step == $xcheckout_to_step)	
			$class = " active loaded";
		else 
			$class = " loaded";
	?>
	<div  id="<?php echo $step; ?>" data-progress="<?php echo $info['progress']?>" data-index="<?php echo $tabindex?>" class="step<?php echo $class;?>" index="<?php echo $tabindex;?>" style="display:block">
		<h4 class="stepTitle"><?php echo "<div class='left_step'><span class='step-count'>$tabindex</span></div> <span class='stepTitleHeading'>{$info['title']}</span>";?></h4>
			<div class="stepContent"<?php if($step != $xcheckout_to_step) echo ' style="display:none"';?>>
			<?php if($load_step && ($xcheckout_to_step != FILENAME_XCHECKOUT_SUCCESS || $step == FILENAME_XCHECKOUT_SUCCESS)){
				$current_page_base = $step;
				//test

				if(in_array('header', $info['load']))
					require(DIR_WS_MODULES."pages/$step/header_php.php");

				//test
				if(in_array('template', $info['load']))				  
					require($template->get_template_dir("tpl_{$step}_default.php",DIR_WS_TEMPLATE, $step ,'templates'). "/tpl_{$step}_default.php"); 
				
				if($step == $xcheckout_to_step)
					$load_step = false;

			}?>
			<div class="clear-both"></div>
		</div>
	</div>
<?php 
$tabindex++;
}

$xcheckout_step = ob_get_clean();
?>
<?php if(\plugins\riPlugin\Plugin::get('settings')->get('riCheckout.progress_box_style') == 'horizontal')
    require($template->get_template_dir('tpl_modules_xcheckout_horizontal_progress.php', DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout'). '/' . 'tpl_modules_xcheckout_horizontal_progress.php');
?>

<div id="output">
<?php
  
/**
 * load all page-specific jscript_*.js files from includes/modules/pages/PAGENAME, alphabetically
 */
  $directory_array = $template->get_template_part(DIR_WS_MODULES . 'pages/' . $xcheckout_to_step, '/^jscript_/', '.js');
  while(list ($key, $value) = each($directory_array)) {
    echo '<script type="text/javascript" src="' . DIR_WS_MODULES . 'pages/' . $xcheckout_to_step . '/' . $value . '"></script>' . "\n";
  }

/**
 * include content from all page-specific jscript_*.php files from includes/modules/pages/PAGENAME, alphabetically.
 */
  $directory_array = $template->get_template_part(DIR_WS_MODULES . 'pages/' . $xcheckout_to_step, '/^jscript_/');
  while(list ($key, $value) = each($directory_array)) {
/**
 * include content from all page-specific jscript_*.php files from includes/modules/pages/PAGENAME, alphabetically.
 * These .PHP files can be manipulated by PHP when they're called, and are copied in-full to the browser page
 */
    require(DIR_WS_MODULES . 'pages/' . $xcheckout_to_step . '/' . $value); echo "\n";
  }  
  if($messageStack->count() > 0){
    echo "<div class='messages'>";
    $messageStack->outputAll();
    echo "</div>";
  }
?>

</div>
<?php 
$css_extra = '';
if(\plugins\riPlugin\Plugin::get('settings')->get('riCheckout.progress_box_style') == 'vertical') {
	$class_extra = " vertical";	
	require($template->get_template_dir('xcheckout/tpl_modules_xcheckout_vertical_progress.php', DIR_WS_TEMPLATE, $current_page_base,'templates'). '/' . 'xcheckout/tpl_modules_xcheckout_vertical_progress.php');
}
?>
<!-- bof tpl_xcheckout_default.php -->
<div id="xcheckout_form">
	<div id="checkout_step" class="xcheckout_progress<?php echo $class_extra;?>">
	<?php echo $xcheckout_step;?>
	</div>		
</div>
<div id="xcheckout_tracking"></div>
</div>
<div style="clear: both;margin:0px 0px 50px 0px"></div>
<!-- eof tpl_xcheckout_default.php -->
</div>