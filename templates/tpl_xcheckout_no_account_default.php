<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=create_account.<br />
 * Displays Create Account form.
 *
 * @package templateSystem
 * @copyright Copyright 2003-2007 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: J_Schilz for Integrated COWOA - 14 April 2007
 */
?>
<?php if(\plugins\riPlugin\Plugin::get('riCheckout.Ajax')->startBlock('xcheckout_step')){?>
<div id="createAcctDefault">
	
<?php echo zen_draw_form('no_account', zen_href_link(FILENAME_XNO_ACCOUNT,'', 'SSL'), 'post') . zen_draw_hidden_field('action', 'process') . zen_draw_hidden_field('email_pref_html', 'email_format'); ?>
	
	
	<div class="clear-both"></div>
<div class="all_field_shipping">
<?php require($template->get_template_dir('tpl_modules_xcheckout_create_account.php',DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout'). '/tpl_modules_xcheckout_create_account.php'); ?>

<?php /* if(\plugins\riPlugin\Plugin::get('settings')->get('riCheckout.is_shipping_address_on_register') == 'true'){ ?>
<?php echo zen_draw_checkbox_field('different_shipping_address', 1, false, 'id="different_shipping_address"') ?>
<label class="checkboxLabel" for="different_shipping_address"><?php echo TEXT_DIFFERENT_ADDRESS; ?></label>
<?php } */?>
</div>
<?php if(\plugins\riPlugin\Plugin::get('settings')->get('riCheckout.is_shipping_address_on_register') == 'true'){ ?>
	<div id="different_shipping_address_fieldset" class="re_billing">
	<center><div class="re_title_billing">Billing Address</div></center>
	<?php
	$address_type = 'billing_';
  require($template->get_template_dir('tpl_modules_xcheckout_new_address2.php',DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout'). '/tpl_modules_xcheckout_new_address2.php'); ?>
  </div>
<?php } ?>
<div class="clear-both"></div>

    <span class="loader" style="display: none;">
    <?php rie('Loading');?>
    </span>
    <button class="xcheckout-next-button btn btn-primary xcheckout-right"><?php rie('Continue')?></button>

</form>
</div>
<script type="text/javascript">
    xcheckout_update_zone(document.no_account, 'payment_');
    xcheckout_update_zone(document.no_account, 'shipping_');
    <?php if(\plugins\riPlugin\Plugin::get('settings')->get('riCheckout.is_shipping_address_on_register') == "true") { ?>
    jQuery(document).ready(function() {
        jQuery("#different_shipping_address").click(function(){
            jQuery("#different_shipping_address_fieldset").toggle();
        });
    });
    <?php }?>
</script>
<?php \plugins\riPlugin\Plugin::get('riCheckout.Ajax')->endBlock();}
require($template->get_template_dir('tpl_modules_xcheckout_tracking.php', DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout'). '/' . 'tpl_modules_xcheckout_tracking.php');
