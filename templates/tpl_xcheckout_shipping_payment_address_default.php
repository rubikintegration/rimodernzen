<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=checkout_payment_address.<br />
 * Allows customer to change the billing address.
 *
 * @package templateSystem
 * @copyright Copyright 2003-2005 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_checkout_payment_address_default.php 4852 2006-10-28 06:47:45Z drbyte $
 */
?>
<?php if(\plugins\riPlugin\Plugin::get('riCheckout.Ajax')->startBlock('xcheckout_step')){?>

<div id="checkoutPayAddressDefault">
<?php echo zen_draw_form('checkout_shipping_payment_address', zen_href_link(FILENAME_XCHECKOUT_SHIPPING_PAYMENT_ADDRESS, '', 'SSL'), 'post', ''); ?>
<?php if($_SESSION['sendto'] !== false) :?>
	<div id="xcheckout_shipping_address_container" class="xcheckout-left">
		<h2 class="title_header"><?php rie('SHIPPING ADDRESS');?></h2>
		<div class="clear-both"></div>
		<?php
		if ($process == false || $error == true) {
			?>

			<?php
			if ($addresses_count > 0) {
				?>

				<?php
				if ($addresses_count < MAX_ADDRESS_BOOK_ENTRIES) { ?>
		<a id="shipping_address_book_toggle"
			href="<?php echo zen_href_link('richeckout_ajax_get_address_entries')?>"
			class="toggle not_ajax">
            <span class="btn btn-address toggle_elements"><?php rie('Add New Address', null, 'riCheckout');?></span>
            <span class="btn btn-address toggle_elements" style="display:none"><?php rie('Use Address Book', null, 'riCheckout');?></span>
		</a>
		<?php }?>

		<div class="shipping_address_book_toggle" id="shipping_address_book">
            <?php echo $riview->render('riCheckout::_address_entries', array('address_type' => 'shipping', 'checkout_addresses' => $checkout_addresses, 'current_address_id' => $current_shipping_address_id));?>
		</div>

		<?php
			}
			?>
		<br class="clear-both" />
		<div class="shipping_address_book_toggle disable_input"
			style="display: none">
			<?php
			if ($addresses_count < MAX_ADDRESS_BOOK_ENTRIES) {

				/**
				 * require template to display new address form
				 */
				$address_type = 'shipping_';
				require($template->get_template_dir('tpl_modules_xcheckout_new_address2.php', DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout'). '/' . 'tpl_modules_xcheckout_new_address2.php');

			}
			?>
		</div>
		<?php
		}

		?>

		<div class="set-xpress back">
		<?php echo zen_draw_checkbox_field('shipping_set_xpress', 1, false, 'id="shipping_set_xpress"') ?>
			<label class="inputLabel2" for="shipping_set_xpress"><?php rie('Express shipping address', null, 'riCheckout'); ?>
				(<a class="wtooltip" title="<?php rie('Choose this as your express shipping address', null, 'riCheckout');?>"
				href="#">?</a>)</label>
		</div>
		<div class="clear-both"></div>
		<div class="buttonRow back">
		<?php echo zen_draw_checkbox_field('same_shipping_payment_address', 1, false, 'id="same_shipping_payment_address"') ?>
			<label class="inputLabel2 ajust" for="same_shipping_payment_address"><?php rie('Express billing address', null, 'riCheckout'); ?>
			</label>
		</div>

	</div>
	<?php endif;?>

	<div id="xcheckout_payment_address_container" class="<?php if($_SESSION['sendto'] !== false) echo 'xcheckout-right'?>">
		<h2 class="title_header"><?php rie('BILLING ADDRESS');?></h2>
		<div class="clear-both"></div>
		<?php //if ($messageStack->size('checkout_address') > 0) echo $messageStack->output('checkout_address'); ?>

		<?php
		if ($addresses_count > 0) {
			?>

			<?php
			if ($addresses_count < MAX_ADDRESS_BOOK_ENTRIES) { ?>
		<a id="payment_address_book_toggle"
			href="<?php echo zen_href_link('richeckout_ajax_get_address_entries')?>"
			class="toggle not_ajax"> <span class="btn toggle_elements btn-address"><?php rie('Add New Address', null, 'riCheckout');?></span>
            <span class="btn toggle_elements btn-address" style="display:none"><?php rie('Use Address Book', null, 'riCheckout');?></span>
		</a>
		<?php } ?>

		<div class="payment_address_book_toggle" id="payment_address_book">
            <?php echo $riview->render('riCheckout::_address_entries', array('address_type' => 'payment', 'checkout_addresses' => $checkout_addresses, 'current_address_id' => $current_payment_address_id));?>
		</div>
		<?php
		}
		?>
		<div class="clear-both"></div>

		<?php
		if ($addresses_count < MAX_ADDRESS_BOOK_ENTRIES) {
			?>
		<div class="payment_address_book_toggle disable_input"
			style="display: none">
			<?php
			/**
			 * require template to collect address details
			 */
			$address_type = 'billing_';
			require($template->get_template_dir('tpl_modules_xcheckout_new_address2.php', DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout'). '/' . 'tpl_modules_xcheckout_new_address2.php');
			?>
		</div>
		<?php
		}
		?>

		<div class="clear-both"></div>

		<div class="set-xpress back">
		<?php echo zen_draw_checkbox_field('payment_set_xpress', 1, false, 'id="payment_set_xpress"') ?>
			<label class="inputLabel2" for="payment_set_xpress"><?php rie('Express billing address', null, 'riCheckout'); ?>
				(<a class="wtooltip" title="<?php rie('Express billing address saves you time and effort by using this address as your default billing address the next time you checkout', null, 'riCheckout');?>" href="#">?</a>)</label>
		</div>
	</div>
	<div class="clear-both"></div>
	<span class="loader" style="display: none;">
    <?php rie('Loading');?>
    </span>
	
	
	<?php echo zen_draw_hidden_field('action', 'submit')?>
    <button class="xcheckout-next-button btn btn-primary xcheckout-right btn-x"><?php rie('Continue')?></button>
    <div class="clear-both"></div>
	</form>
</div>
<script type="text/javascript"    >
    xcheckout_update_zone(document.checkout_shipping_payment_address, 'payment_');
    xcheckout_update_zone(document.checkout_shipping_payment_address, 'shipping_');
</script>
<?php \plugins\riPlugin\Plugin::get('riCheckout.Ajax')->endBlock();}
require($template->get_template_dir('tpl_modules_xcheckout_tracking.php', DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout'). '/' . 'tpl_modules_xcheckout_tracking.php');