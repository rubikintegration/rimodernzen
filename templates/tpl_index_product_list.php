<?php
//get product by categories
$record_per_page = (isset($_GET['record_per_page']) && (int)$_GET['record_per_page'] > 0) ? (int)$_GET['record_per_page'] : plugins\riPlugin\Plugin::get('settings')->get('theme.listing.record_per_page');

echo $riview->render('riElement::frontend/product_listing/style1/_products.php', array(
    'title' => $breadcrumb->last(),
    'current_route' => 'index',
    'listing_sql' => $listing_sql,
    'record_per_page' => $record_per_page,
    'column_list' => $column_list)
);


