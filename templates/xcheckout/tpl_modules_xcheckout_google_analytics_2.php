<?php
/**
 * tpl_footer_googleanalytics.php
 *
 * @package zen-cart analytics
 * @copyright Copyright 2004-2008 Andrew Berezin eCommerce-Service.com
 * @copyright Copyright 2003-2007 Zen Cart Development Team
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_footer_googleanalytics.php, v 2.2.1 01.09.2008 01:23 Andrew Berezin $
 */
	// http://www.zen-cart.com/forum/showpost.php?p=376521&postcount=254
	@define('GOOGLE_ANALYTICS_PRODUCTS_ATTRIBUTES_BRACKETS', '[]');
	@define('GOOGLE_ANALYTICS_PRODUCTS_ATTRIBUTES_DELIMITER', '; ');
	@define('GOOGLE_ANALYTICS_USE_PAGENAME', 'false');
	// http://www.google.com/support/googleanalytics/bin/answer.py?hl=ru&answer=55503
	// http://www.google.com/support/googleanalytics/bin/answer.py?hl=ru&answer=55532
	// http://www.google.com/support/googleanalytics/bin/answer.py?hl=ru&answer=55524
	@define('GOOGLE_ANALYTICS_DOMAINNAME', '');
	@define('GOOGLE_ANALYTICS_ALLOWLINKER', 'false');
	
	@define('GOOGLE_CONVERSION_ACTIVE', 'true');
	@define('GOOGLE_CONVERSION_ID', '');
	@define('GOOGLE_CONVERSION_LANGUAGE', 'en_EN');
	
	if(GOOGLE_ANALYTICS_UACCT != '') {
	
	// http://www.google.ru/support/googleanalytics/bin/answer.py?answer=55480
	// http://www.google.ru/support/googleanalytics/bin/static.py?page=troubleshooter.cs&problem=tracking&selected=tracking_php&ctx=tracking_tracking_php_55504
	if(GOOGLE_ANALYTICS_USE_PAGENAME == 'true') {
	  //$google_analytics_page = '"' . zen_output_string_protected($breadcrumb->last()) . (isset($_GET['page']) ? ' (' . sprintf(PREVNEXT_TITLE_PAGE_NO, $_GET['page']) . ')' : '') . '"';
	  $google_analytics_page = $_SERVER['REQUEST_URI'];
	} else {
	  $google_analytics_page = '';
	}
	

	?>
	<script type="text/javascript">
	<!--
	if(!gaJsHost) {
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	}
	//-->
	</script>
	
	<script type="text/javascript">
	<!--
	if(!pageTracker) {
		var pageTracker = _gat._getTracker('<?php echo GOOGLE_ANALYTICS_UACCT; ?>');
	}
	<?php if(GOOGLE_ANALYTICS_DOMAINNAME != '') { ?>
	pageTracker._setDomainName('<?php echo GOOGLE_ANALYTICS_DOMAINNAME; ?>');
	<?php } ?>
	<?php if(GOOGLE_ANALYTICS_ALLOWLINKER == 'true') { ?>
	pageTracker._setAllowLinker(true);
	<?php } ?>
	pageTracker._initData();
	pageTracker._trackPageview('<?php echo $google_analytics_page; ?>');
	
	<?php
	// http://www.google.ru/support/googleanalytics/bin/answer.py?answer=55528
	if($xcurrent_page_base == FILENAME_XCHECKOUT_SUCCESS && (!isset($_SESSION['google_analytics']) || !in_array($zv_orders_id, $_SESSION['google_analytics']))) {
	  if(!isset($_SESSION['google_analytics'])) $_SESSION['google_analytics'] = array();
	  $_SESSION['google_analytics'][] = $zv_orders_id;
	  $_trackTrans = '';
	  require_once(DIR_WS_CLASSES . 'order.php');
	  $order = new order($zv_orders_id);
	//echo '<pre>';var_export($order);echo '</pre>';
	  switch (GOOGLE_ANALYTICS_TARGET) {
	    case 'delivery':
	      $google_analytics = $order->delivery;
	      break;
	    case 'billing':
	      $google_analytics = $order->billing;
	      break;
	    case 'customers':
	    default:
	      $google_analytics = $order->customer;
	      break;
	  }
	
	  $google_analytics['ot_shipping'] = 0;
	  $totals = $db->Execute("SELECT value
	                          FROM " . TABLE_ORDERS_TOTAL . "
	                          WHERE orders_id = '" . (int)$zv_orders_id . "'
	                            AND class = 'ot_shipping'");
	  if (!$totals->EOF) {
	    $google_analytics['ot_shipping'] = $totals->fields['value'];
	  }
	/*
	  foreach ($order->totals as $k => $v) {
	    var_dump($v['class'], $v['text']);echo '<br />';
	    if($v['class'] == '') {
	      $google_analytics['ot_shipping'] = float($v['text']);
	      break;
	    }
	  }
	*/
	//  pageTracker._addTrans(Order_ID, Affiliation, Total, Tax, Shipping, City, State, Country);
	  $_trackTrans .= '  pageTracker._addTrans(' .
	                  '"' . $zv_orders_id . '",' .
	                  '"' . GOOGLE_ANALYTICS_AFFILIATION . '",' .
	                  '"' . number_format($order->info['total'], 3, '.', '') . '",' .
	                  '"' . number_format($order->info['tax'], 3, '.', '') . '",' .
	                  '"' . number_format($google_analytics['ot_shipping'], 3, '.', '') . '",' .
	                  '"' . zen_output_string_protected($google_analytics['city']) . '",' .
	                  '"' . zen_output_string_protected($google_analytics['state']) . '",' .
	                  '"' . zen_output_string_protected($google_analytics['country']) . '");' . "\n";
	
	  for ($i=0; $i<sizeof($order->products); $i++) {
	/*
	    $category_query = "SELECT cd.categories_name
	                       FROM " . TABLE_PRODUCTS . " p
	                         LEFT JOIN " . TABLE_CATEGORIES_DESCRIPTION . " cd ON (cd.categories_id = p.master_categories_id)
	                       WHERE p.products_id = :productsID
	                         AND cd.language_id = :languagesID
	                       LIMIT 1";
	    $category_query = $db->bindVars($category_query, ':languagesID', $_SESSION['languages_id'], 'integer');
	    $category_query = $db->bindVars($category_query, ':productsID', zen_get_prid($order->products[$i]['id']), 'integer');
	    $category = $db->Execute($category_query);
	    $categories_name = $category->fields['categories_name'];
	*/
	    $categories_name = zen_get_categories_name_from_product(zen_get_prid($order->products[$i]['id']));
	    if(GOOGLE_ANALYTICS_SKUCODE == 'products_model') {
	      $products_skucode = $order->products[$i]['model'];
	    } else {
	      $products_skucode = $order->products[$i]['id'];
	    }
	    $products_attributes_name = '';
	    if (isset($order->products[$i]['attributes'])) {
	      for ($j=0; $j<sizeof($order->products[$i]['attributes']); $j++) {
	        $products_attributes_name .= $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'] . GOOGLE_ANALYTICS_PRODUCTS_ATTRIBUTES_DELIMITER;
	      }
	      $products_attributes_name = substr(GOOGLE_ANALYTICS_PRODUCTS_ATTRIBUTES_BRACKETS, 0, 1) . rtrim($products_attributes_name, GOOGLE_ANALYTICS_PRODUCTS_ATTRIBUTES_DELIMITER) . substr(GOOGLE_ANALYTICS_PRODUCTS_ATTRIBUTES_BRACKETS, 1, 1);
	    }
	//   pageTracker._addItem(Order_ID, SKU, Product_Name, Category, Price, Quantity);
	    $_trackTrans .= '  pageTracker._addItem(' .
	                    '"' . $zv_orders_id . '",' .
	                    '"' . zen_output_string_protected($products_skucode) . '",' .
	                    '"' . zen_output_string_protected($order->products[$i]['name'] . $products_attributes_name) . '",' .
	                    '"' . zen_output_string_protected($categories_name) . '",' .
	                    '"' . number_format($order->products[$i]['final_price'], 2, '.', '') . '",' .
	                    '"' . $order->products[$i]['qty'] . '");' . "\n";
	  }
	  $_trackTrans .= 'pageTracker._trackTrans();' . "\n";
	  echo $_trackTrans;
	}
	?>
	//-->
	</script>
	
	<?php
	}
	?>
	<?php
	if($xcurrent_page_base == FILENAME_XCHECKOUT_SUCCESS && GOOGLE_CONVERSION_ACTIVE == 'true' && trim(GOOGLE_CONVERSION_ID) != '' && isset($order->info['total'])) {
	  if ($request_type == 'NONSSL') {
	    $googleadservices_scheme = "http";
	  } else {
	    $googleadservices_scheme = "https";
	  }
	  if($order->info['total'] > 0) {
	    $google_conversion_value = number_format($order->info['total'], 3, '.', '');
	  } else {
	    $google_conversion_value = 1;
	  }
	?>
	<script type="text/javascript">
	<!--
	var google_conversion_id = <?php echo GOOGLE_CONVERSION_ID; ?>;
	var google_conversion_language = "<?php echo GOOGLE_CONVERSION_LANGUAGE; ?>";
	var google_conversion_format = "1";
	var google_conversion_color = "FFFFFF";
	if (<?php echo $google_conversion_value; ?>) {
	  var google_conversion_value = <?php echo $google_conversion_value; ?>;
	}
	var google_conversion_label = "Purchase";
	//-->
	</script>
	<?php 
	$google_conversion_url = $googleadservices_scheme . '://www.googleadservices.com/pagead/conversion.js';
	if(!\plugins\riPlugin\Plugin::get('riCheckout.Ajax')->status()){
		echo '<script language="JavaScript" src="' . $google_conversion_url . '"></script>';
	}
	else
		\plugins\riPlugin\Plugin::get('riCheckout.Ajax')->json()->set(array("jscript"=>"$google_conversion_url"), true);
	?>

	<noscript><img height=1 width=1 border=0 src="<?php echo $googleadservices_scheme . '://www.googleadservices.com/pagead/conversion/' . GOOGLE_CONVERSION_ID . '/?value=' . $google_conversion_value . '&label=Purchase&script=0'; ?>" /></noscript>
	<?php
	}