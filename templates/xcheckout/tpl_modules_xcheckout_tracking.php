<?php if(\plugins\riPlugin\Plugin::get('riCheckout.Ajax')->startBlock('xcheckout_tracking')){
/**
 * BELOW IS THE SAMPLE CODE FOR e-concept's Google Analytics MODULE
 * UNCOMMENT AND MAKE CHANGES TO USE
 * IF YOU USE ANOTHER Google Analytics YOU CAN PUT ITS CODE HERE
 */

/**
 * This is for EConcept's Google Tracking
 */
	
if(\plugins\riPlugin\Plugin::get('settings')->get('riCheckout.is_ga_module_1_status') == 'true')
require($template->get_template_dir('.php',DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout') . '/tpl_modules_xcheckout_google_analytics_1.php');

/**
 * This is for A.Berezin's Google Tracking
 */
elseif(\plugins\riPlugin\Plugin::get('settings')->get('riCheckout.is_ga_module_2_status') == 'true')
require($template->get_template_dir('.php',DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout') . '/tpl_modules_xcheckout_google_analytics_2.php');

/**
 * Turn on JAM tracking if this has been installed
 */
if(isset($JAM) && is_object($JAM)){
	##########################################
	## START JAM INTEGRATION WITH ZEN CART  ##
	## ZC Integration code by DrByte 8/2006 ##
	##########################################
	if ((int)$orders_id > 0) {
	  $JAM = $db->Execute("select class, value from " . TABLE_ORDERS_TOTAL . " where orders_id = '".(int)$orders_id."' AND class in ('ot_coupon', 'ot_subtotal', 'ot_group_pricing')");
	  while (!$JAM->EOF) {
	    switch ($JAM->fields['class']) {
	      case 'ot_subtotal':
	       $order_subtotal = $JAM->fields['value'];
	        break;
	      case 'ot_coupon':
	       $coupon_amount = $JAM->fields['value'];
	        break;
	      case 'ot_group_pricing':
	       $group_pricing_amount = $JAM->fields['value'];
	        break;
	    }
	    $JAM->MoveNext();
	  }
	  $commissionable_order = ($order_subtotal - $coupon_amount - $group_pricing_amount);
	  $commissionable_order = number_format($commissionable_order,2,'.',',');
	  // replace http://yoursite.com/affiliates/ by the path to your affiliate program
	echo "<script language=\"JavaScript\" type=\"text/javascript\" src=\"http://yoursite.com/affiliates/sale.php?amount=$commissionable_order&trans_id=$orders_id\"></script></td></tr>
	</table>";
	}
	
	#######################################
	## END JAM INTEGRATION WITH ZEN CART ##
	#######################################
}
\plugins\riPlugin\Plugin::get('riCheckout.Ajax')->endBlock();}