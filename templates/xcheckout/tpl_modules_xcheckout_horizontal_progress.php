<?php if($xcurrent_page_base != 'xcheckout_login'):?>

<?php \plugins\riPlugin\Plugin::get('riCheckout.Ajax')->loadLanguage(FILENAME_XCHECKOUT); ?>

<?php $content = array();?>

<?php 
$content['shipping_payment_address']['content'] = '<table><tr>';
if(isset($_SESSION['customer_id']) && isset($_SESSION['sendto']) && $_SESSION['sendto'] && $xcurrent_page_base != FILENAME_XCHECKOUT_SUCCESS){
  
  $content['shipping_payment_address']['class'] = ($xcurrent_page_base == FILENAME_XCHECKOUT_SHIPPING_PAYMENT_ADDRESS) ? ' progress_content current' : ' progress_content loaded';
	// if shipping-edit button should be overridden, do so
  if (isset($_SESSION['payment']) && method_exists($$_SESSION['payment'], 'alterShippingEditButton')) {
    $theLink = $$_SESSION['payment']->alterShippingEditButton();
    if ($theLink) {
      $content['shipping_payment_address']['link'] = '<span class="step_links_container">(<a class="not_ajax step_links" href="'.$theLink.'">'.ri('edit', null, 'riCheckout').'</a>)</span>';
    }
  }
  else{
  	$content['shipping_payment_address']['link'] = '<span class="step_links_container">(<a class="not_ajax step_links" href="'.FILENAME_XCHECKOUT_SHIPPING_PAYMENT_ADDRESS.'">'.ri('edit', null, 'riCheckout').'</a>)</span>';
  }
  $content['shipping_payment_address']['content'] .= '<td id="sendto_address"><div class="heading">'.ri('Shipping address', null, 'riCheckout').'</div><address>'.zen_address_label($_SESSION['customer_id'], $_SESSION['sendto'], true, ' ', '<br />').'</address></td>';
}

if(isset($_SESSION['customer_id']) && isset($_SESSION['billto']) && $xcurrent_page_base != FILENAME_XCHECKOUT_SUCCESS){
	$content['shipping_payment_address']['class'] = ($xcurrent_page_base == FILENAME_XCHECKOUT_SHIPPING_PAYMENT_ADDRESS) ? ' progress_content current' : ' progress_content loaded';
	$content['shipping_payment_address']['link'] = '<span class="step_links_container">(<a class="not_ajax step_links" href="'.FILENAME_XCHECKOUT_SHIPPING_PAYMENT_ADDRESS.'">'.ri('edit', null, 'riCheckout').'</a>)</span>';
	$content['shipping_payment_address']['content'] .= '<td id="billto_address"><div class="heading">'.ri('Billing Address').'</div><address>'.zen_address_label($_SESSION['customer_id'], $_SESSION['billto'], true, ' ', '<br />').'</address></td>';
}

$content['shipping_payment_address']['content'] = $content['shipping_payment_address']['content'].'</tr></table>';
?>

<?php 
if($_SESSION['sendto']!== false && $xcurrent_page_base != FILENAME_XCHECKOUT_SUCCESS){
	$content['shipping_payment']['class'] = ($xcurrent_page_base == FILENAME_XCHECKOUT_SHIPPING_PAYMENT) ? ' progress_content current' : ' progress_content loaded';
	$content['shipping_payment']['link'] = '<span class="step_links_container">(<a class="not_ajax step_links" href="'.FILENAME_XCHECKOUT_SHIPPING_PAYMENT.'">'.ri('edit', null, 'riCheckout').'</a>)</span>';
	
	$content['shipping_payment']['content'] = '<div class="title_summary_shipping">'.$_SESSION['shipping']['title'].'</div>';
	
}

if(isset($_SESSION['payment']) && $xcurrent_page_base != FILENAME_XCHECKOUT_SUCCESS){
	$content['shipping_payment']['class'] = ($xcurrent_page_base == FILENAME_XCHECKOUT_SHIPPING_PAYMENT) ? ' progress_content current' : ' progress_content loaded';
	$content['shipping_payment']['link'] = '<span class="step_links_container">(<a class="not_ajax step_links" href="'.FILENAME_XCHECKOUT_SHIPPING_PAYMENT.'">'.ri('edit', null, 'riCheckout').'</a>)</span>';
	if(!isset($payment_modules)) {
		require_once(DIR_WS_CLASSES . 'payment.php');
		$payment_modules = new payment($_SESSION['payment']);
		$payment_modules->update_status();
	}
	
	$content['shipping_payment']['content'] .= '<div class="title_summary_payment">'.$payment_modules->paymentClass->title.'</div>';
}

?>

<?php 
if($_SESSION['customer_id'] && MODULE_ORDER_TOTAL_INSTALLED && isset($order) && $xcurrent_page_base != FILENAME_XCHECKOUT_SUCCESS){
	if(!isset($order_total_modules) || !is_object($order_total_modules)){
	require_once(DIR_WS_CLASSES . 'order_total.php');
	$order_total_modules = new order_total;
	$order_totals = $order_total_modules->process();
	}
		
	$content['total']['class'] = ($xcurrent_page_base == FILENAME_XCHECKOUT_CONFIRMATION) ? ' progress_content current' : ' progress_content loaded';
	}
	if(is_object($order_total_modules)){
	  ob_start();
	  $order_total_modules->output();
	  $content['total']['content'] = ob_get_clean();
	}
?>

<table id="xcheckout_progress">
  <?php if(\plugins\riPlugin\Plugin::get('riCheckout.Ajax')->startBlock('xcheckout_progress')) : ?>
  <tr>
    <th data-step="<?php echo FILENAME_XCHECKOUT_SHIPPING_PAYMENT_ADDRESS?>" class="xcheckout_shipping_payment_address_proccess<?php echo $content['shipping_payment_address']['class']?>">
    <h4 class="title"><?php rie('Shipping/Billing Address', null, 'riCheckout'); ?> <?php echo $content['shipping_payment_address']['link']; ?></h4>
    </th>
    <th data-step="<?php echo FILENAME_XCHECKOUT_SHIPPING_PAYMENT?>" class="xcheckout_shipping_payment_proccess<?php echo $content['shipping_payment']['class']?>">
    <h4 class="title"><?php rie('Shipping/Billing Method', null, 'riCheckout'); ?> <?php echo $content['shipping_payment']['link']; ?></h4>
    </th>
    <th data-step="<?php echo FILENAME_XCHECKOUT_CONFIRMATION?>" class="xcheckout_order_total_proccess<?php echo $content['total']['class']?>">
    <h4 class="title"><?php rie('Order Total', null, 'riCheckout'); ?> <?php echo $content['total']['link']; ?></h4>
    </th>
  </tr>
  <tr>
    <td class="xcheckout_shipping_payment_address_proccess<?php echo $content['shipping_payment_address']['class']?>">
    <?php echo $content['shipping_payment_address']['content']?>
    </td>
    <td class="xcheckout_shipping_payment_proccess<?php echo $content['shipping_payment']['class']?>">
    <?php echo $content['shipping_payment']['content']?><div class="clear-both"></div>
    </td>
    <td class="xcheckout_order_total_proccess<?php echo $content['total']['class']?>">
    <?php echo $content['total']['content']?>
    </td>
  </tr>
  <?php \plugins\riPlugin\Plugin::get('riCheckout.Ajax')->endBlock(); endif; ?>
</table>
<?php endif;?>