<?php
/**
 * tpl_block_checkout_shipping_address.php
 *
 * @package templateSystem
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_modules_checkout_address_book.php 3101 2006-03-03 05:56:23Z drbyte $
 */
?>
<?php
/**
 * require code to get address book details
 */
  require(DIR_WS_MODULES . zen_get_module_directory('checkout_address_book.php'));
?>

<?php
			$address_counter = 0;
			$checkout_addresses = array();
			while (!$addresses->EOF) {
           	
      	if ($addresses->fields['address_book_id'] == $_SESSION['sendto']) {
      	 $current_address_id = $addresses->fields['address_book_id'];
  			 
        }

					$checkout_addresses[] = array('id' => $addresses->fields['address_book_id'], 'text' => zen_address_format($format_id, $addresses->fields, true, ' ', ''));        
          
				$addresses->MoveNext();
      }
      
      echo zen_draw_pull_down_menu('address', $checkout_addresses, $current_address_id);
?>