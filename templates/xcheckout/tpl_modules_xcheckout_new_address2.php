<?php
/**
 * Module Template
 *
 * Allows entry of new addresses during checkout stages
 *
 * @package templateSystem
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_modules_checkout_new_address.php 4683 2006-10-07 06:11:53Z drbyte $
 */

?>
<div class="centerColumnModule" id="<?php echo $address_type;?>checkoutNewAddress">

    <div class="xcheckout-right"><span class="x-alert">* <?php rie('Required'); ?></span></div>
    <div class="clear-both"></div>


    <div class="xcheckout-left">

        <?php
        if (ACCOUNT_GENDER == 'true') {
            ?><div class="field_account">
                <label class="inputLabel" for="<?php echo $address_type;?>gender"><?php rie('Gender') ;echo  (zen_not_null(ENTRY_GENDER_TEXT) ? '<span class="x-alert">' . ENTRY_GENDER_TEXT . ' </span>:': ':'); ?></label>
                <div class="clear-both"></div>
                <?php echo zen_draw_radio_field($address_type.'gender', 'm', '', "id='{$address_type}gender-male'") . '<label class="radioButtonLabel" for="'.$address_type.'gender-male">' . MALE . '</label>' . zen_draw_radio_field($address_type.'gender', 'f', '', "id='{$address_type}gender-female'") . '<label class="radioButtonLabel" for="'.$address_type.'gender-female">' . FEMALE . '</label>'; ?>
                <label for="<?php echo $address_type;?>gender" class="error"><?php echo ENTRY_GENDER_ERROR;?></label>
            </div>
            <div class="clear-both"></div>
            <?php
        }
        ?>


        <?php
        if (ACCOUNT_COMPANY == 'true') {
            ?>
            <div class="field_account">
                <label class="inputLabel" for="<?php echo $address_type;?>company"><?php echo ENTRY_COMPANY . (zen_not_null(ENTRY_COMPANY_TEXT) ? '<span class="x-alert">' . ENTRY_COMPANY_TEXT . '</span>': ''); ?></label>
                <div class="clear-both"></div>
                <?php echo zen_draw_input_field($address_type.'company', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_company', '40') . ' id="'.$address_type.'company"'); ?>
            </div>
            <div class="clear-both"></div>


            <!-- TVA_INTRACOM BEGIN //-->
            <?php if(\plugins\riPlugin\Plugin::get('settings')->get('riCheckout.is_eu_vat_installed') == 'true') { ?>
                <div class="field_account">
                    <label class="inputLabel" for="<?php echo $address_type;?>tva_intracom"><?php echo ENTRY_TVA_INTRACOM . (zen_not_null(ENTRY_TVA_INTRACOM_TEXT) ? '<span class="x-alert">' . ENTRY_TVA_INTRACOM_TEXT . '</span>': ''); ?></label>
                    <div class="clear-both"></div>
                    <?php echo zen_draw_input_field($address_type.'tva_intracom', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_tva_intracom', '40') . ' id="'.$address_type.'tva_intracom"'); ?>
                </div>
                <div class="clear-both"></div>
                <?php } ?>
            <!-- TVA_INTRACOM END //-->
            <?php
        }
        ?>

        <div class="field_account">
            <label class="inputLabel" for="<?php echo $address_type;?>firstname"><?php echo ENTRY_FIRST_NAME . (zen_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="x-alert">' . ENTRY_FIRST_NAME_TEXT . '</span>': ''); ?></label>
            <div class="clear-both"></div>
            <?php echo zen_draw_input_field($address_type.'firstname', '', zen_set_field_length(TABLE_CUSTOMERS, 'customers_firstname', '40') . ' id="'.$address_type.'firstname"'); ?>
        </div>
        <div class="clear-both"></div>


        <div class="field_account">
            <label class="inputLabel" for="<?php echo $address_type;?>lastname"><?php echo ENTRY_LAST_NAME . (zen_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="x-alert">' . ENTRY_LAST_NAME_TEXT . '</span>': ''); ?></label>
            <div class="clear-both"></div>
            <?php echo zen_draw_input_field($address_type.'lastname', '', zen_set_field_length(TABLE_CUSTOMERS, 'customers_lastname', '40') . ' id="'.$address_type.'lastname"'); ?>
        </div>
        <div class="clear-both"></div>


        <div class="field_account">
            <label class="inputLabel" for="<?php echo $address_type;?>street-address"><?php echo ENTRY_STREET_ADDRESS . (zen_not_null(ENTRY_STREET_ADDRESS_TEXT) ? '<span class="x-alert">' . ENTRY_STREET_ADDRESS_TEXT . '</span>': ''); ?></label>
            <div class="clear-both"></div>
            <?php echo zen_draw_input_field($address_type.'street_address', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_street_address', '40') . ' id="'.$address_type.'street-address"'); ?>
        </div>
        <div class="clear-both"></div>


        <?php
        if (ACCOUNT_SUBURB == 'true') {
            ?>
            <div class="field_account">
                <label class="inputLabel" for="<?php echo $address_type;?>suburb"><?php echo ENTRY_SUBURB . (zen_not_null(ENTRY_SUBURB_TEXT) ? '<span class="x-alert">' . ENTRY_SUBURB_TEXT . '</span>': ''); ?></label>
                <div class="clear-both"></div>
                <?php echo zen_draw_input_field($address_type.'suburb', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_suburb', '40') . ' id="'.$address_type.'suburb"'); ?>
            </div>
            <div class="clear-both"></div>
            <?php
        }
        ?>


    </div> <!-- left side -->



    <div>
        <div>

            <div class="field_account mini-input xcheckout-left">
                <label class="inputLabel" for="<?php echo $address_type;?>country"><?php echo ENTRY_COUNTRY . (zen_not_null(ENTRY_COUNTRY_TEXT) ? '<span class="x-alert">' . ENTRY_COUNTRY_TEXT . '</span>': ''); ?></label>
                <div class="clear-both"></div>
                <?php echo zen_get_country_list($address_type.'zone_country_id', ${$address_type.'selected_country'}, 'class="custom_country"'.'id="'.$address_type.'country" ' . (${$address_type.'flag_show_pulldown_states'} == true ? 'onchange="xcheckout_update_zone(this.form, "' . $address_type .'");"' : '')); ?>
            </div>
            <div class="clear-both"></div> <!-- Country -->



            <div class="field_account mini-input">
                <label class="inputLabel" for="<?php echo $address_type;?>postcode"><?php echo ENTRY_POST_CODE . (zen_not_null(ENTRY_POST_CODE_TEXT) ? '<span class="x-alert">' . ENTRY_POST_CODE_TEXT . '</span>': ''); ?></label>
                <div class="clear-both"></div>
                <?php echo zen_draw_input_field($address_type.'postcode', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_postcode', '40') . ' id="'.$address_type.'postcode"'); ?>
            </div>
            <!-- Post Code -->


            <?php
            if (ACCOUNT_STATE == 'true') {
                if (${$address_type.'flag_show_pulldown_states'} == true) {
                    ?>
                    <div class="field_account mini-input">
                        <label class="inputLabel" for="<?php echo $address_type;?>stateZone" id="<?php echo $address_type;?>zoneLabel"><?php echo ENTRY_STATE; if (zen_not_null(ENTRY_STATE_TEXT)) echo '&nbsp;<span class="x-alert">' . ENTRY_STATE_TEXT . '</span>'; ?></label>
                        <div class="clear-both"></div>
                        <?php
                        echo zen_draw_pull_down_menu($address_type.'zone_id', zen_prepare_country_zones_pull_down(${$address_type.'selected_country'}), $zone_id, 'id="'.$address_type.'stateZone"');

                        ?>
                    </div>
                    <div id="<?php echo $address_type;?>stBreak" ></div>
                    <?php } ?>


                <div class="field_account mini-input xcheckout-left">
                    <label class="inputLabel" for="<?php echo $address_type;?>state" id="<?php echo $address_type;?>stateLabel">State<?php echo ${$address_type.'state_field_label'};if (zen_not_null(ENTRY_STATE_TEXT)) echo '&nbsp;<span class="x-alert" id="'.$address_type.'stText">' . ENTRY_STATE_TEXT . '</span>'; ?></label>
                    <div class="clear-both"></div>
                    <?php
                    echo zen_draw_input_field($address_type.'state', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_state', '40') . ' id="'.$address_type.'state"');
                    if (${$address_type.'flag_show_pulldown_states'} == false) {
                        echo zen_draw_hidden_field($address_type.'zone_id', ${$address_type.'zone_name'}, ' ');
                    }
                    ?>
                </div> <!-- STATE -->
                <?php } ?> 
            <div class="clear-both"></div>


            <div class="field_account xcheckout-left">
                <label class="inputLabel" for="<?php echo $address_type;?>city"><?php echo ENTRY_CITY . (zen_not_null(ENTRY_CITY_TEXT) ? '<span class="x-alert">' . ENTRY_CITY_TEXT . '</span>': ''); ?></label>
                <div class="clear-both"></div>
                <?php echo zen_draw_input_field($address_type.'city', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_city', '40') . ' id="'.$address_type.'city"'); ?>
            </div> <!-- City -->


            <!--
           <div class="field_account">
               <label class="inputLabel" for="telephone"><a?php echo ENTRY_TELEPHONE_NUMBER . (zen_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '<span class="x-alert">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>': ''); ?></label>
               <div class="clear-both"></div>
               <a?php echo zen_draw_input_field($address_type.'telephone', '', zen_set_field_length(TABLE_ADDRESS_BOOK, $address_type.'entry_telephone', '40') . " id='{$address_type}telephone'"); ?>
           </div>
           <div class="clear-both"></div>
           -->

        </div>
    </div> <!-- right side -->


    <div class="clear-both"></div>
</div>