<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=create_account.<br />
 * Displays Create Account form.
 *
 * @package templateSystem
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_modules_create_account.php 4822 2006-10-23 11:11:36Z drbyte $
 */
?>

<?php //if ($messageStack->size('create_account') > 0) echo $messageStack->output('create_account'); ?>
<div id="registry" class="centerColumnModule">
	<fieldset>
	<div class="re_title_billing"><?php rie('YOUR INFORMATION DETAIL', null, 'riCheckout');?></div>
	<div class="xcheckout-right"><span class="x-alert">* <?php rie('Required', null, 'riCheckout'); ?></span></div>
	<div class="clear-both"></div>
	
	<div class="group group_shipping">
	
		<div class="xcheckout-left">
			<?php
			  if (DISPLAY_PRIVACY_CONDITIONS == 'true') {
			?>	<div class="field_account">
					<div class="information">
						<?php rie('Please acknowledge you agree with our privacy statement by ticking the following box. The privacy statement can be read <a href="%link%"><span class="pseudolink">here</span></a>', array("%link%" => zen_href_link(FILENAME_PRIVACY, '', 'SSL')), 'riCheckout');?>
					</div>
					<?php echo zen_draw_checkbox_field('privacy_conditions', '1', false, 'id="privacy"');?>
					<label class="checkboxLabel" for="privacy"><?php rie('I have read and agreed to your privacy statement.', null, 'riCheckout');?></label>
				</div> <div class="clear-both"></div>
			<?php	}	?>
			
			<?php
			  if (ACCOUNT_COMPANY == 'true') {
			?>	
			
			<div class="field_account">
				<label class="inputLabel" for="company"><?php echo ri('Company Name:', null, 'riCheckout'). (zen_not_null(ENTRY_COMPANY_TEXT) ? '<span class="x-alert">' . ENTRY_COMPANY_TEXT . '</span>': ''); ?></label>
			<div class="clear-both"> </div>
				
			<?php echo zen_draw_input_field('company', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_company', '40') . ' id="company"') ; ?>
			<div class="clear-both"> </div>
	
				<!-- TVA_INTRACOM BEGIN //-->
				<?php if(\plugins\riPlugin\Plugin::get('settings')->get('riCheckout.is_eu_vat_installed') == 'true') { ?>
							<label class="inputLabel" for="tva_intracom"><?php echo ENTRY_TVA_INTRACOM . (zen_not_null(ENTRY_TVA_INTRACOM_TEXT) ? '<span class="x-alert">' . ENTRY_TVA_INTRACOM_TEXT . '</span>': ''); ?></label>
							<?php echo zen_draw_input_field('tva_intracom', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_tva_intracom', '40') . ' id="tva_intracom"') ; ?>
				<?php } ?>
				<!-- TVA_INTRACOM END //-->
			</div>
			<div class="clear-both"> </div>
			<?php
			  }
			?>
			
			<?php
			  if (ACCOUNT_GENDER == 'true') {
			?>	
			<div class="field_account">
					<label class="inputLabel" for="gender"><?php echo ri('Gender') . (zen_not_null(ENTRY_GENDER_TEXT) ? '<span class="x-alert">' . ENTRY_GENDER_TEXT . ' </span>:': ':'); ?></label>
					<div class="clear-both"> </div>
					<?php echo zen_draw_radio_field('gender', 'm', '', 'id="gender-male"') . '<label class="radioButtonLabel" for="gender-male">' . MALE . '</label>' . zen_draw_radio_field('gender', 'f', '', 'id="gender-female"') . '<label class="radioButtonLabel" for="gender-female">' . FEMALE . '</label>'; ?>
					<label class="error" for="gender"><?php echo ENTRY_GENDER_ERROR;?></label>
			</div>
			<div class="clear-both"> </div>
			<?php
			  }
			?>
			
			<div class="field_account">
				<label class="inputLabel" for="firstname"><?php echo ENTRY_FIRST_NAME . (zen_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="x-alert">' . ENTRY_FIRST_NAME_TEXT . '</span>': ''); ?></label>
				<div class="clear-both"> </div>
				<?php echo zen_draw_input_field('firstname', '', zen_set_field_length(TABLE_CUSTOMERS, 'customers_firstname', '40') . ' id="firstname"') ; ?>
			</div>
			<div class="clear-both"> </div>
		
			<div class="field_account">
				<label class="inputLabel" for="lastname"><?php echo ENTRY_LAST_NAME . (zen_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="x-alert">' . ENTRY_LAST_NAME_TEXT . '</span>': ''); ?></label>
			<div class="clear-both"> </div>
			
			<?php echo zen_draw_input_field('lastname', '', zen_set_field_length(TABLE_CUSTOMERS, 'customers_lastname', '40') . ' id="lastname"'); ?>
			</div>
			<div class="clear-both"> </div>

			<div class="field_account">
				<label class="inputLabel" for="street-address"><?php echo ENTRY_STREET_ADDRESS . (zen_not_null(ENTRY_STREET_ADDRESS_TEXT) ? '<span class="x-alert">' . ENTRY_STREET_ADDRESS_TEXT . '</span>': ''); ?></label>
			<div class="clear-both"> </div>
			  <?php echo zen_draw_input_field('street_address', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_street_address', '40') . ' id="street-address"'); ?>
			</div>
			<div class="clear-both"> </div>		
			
			<?php
			  if (ACCOUNT_SUBURB == 'true') {
			?>
			<div class="field_account">
				<label class="inputLabel" for="suburb"><?php echo ENTRY_SUBURB . (zen_not_null(ENTRY_SUBURB_TEXT) ? '<span class="x-alert">' . ENTRY_SUBURB_TEXT . '</span>': ''); ?></label>
				<div class="clear-both"> </div>
				<?php echo zen_draw_input_field('suburb', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_suburb', '40') . ' id="suburb"'); ?>
			</div>
			<div class="clear-both"> </div>
				<?php
			  }
			?>
		</div> <!--eof xcheckout-left -->
			
		
		<div class="xcheckout-right">
			
			<div class="field_account mini-input back">
				<label class="inputLabel" for="country"><?php echo ENTRY_COUNTRY . (zen_not_null(ENTRY_COUNTRY_TEXT) ? '<span class="x-alert">' . ENTRY_COUNTRY_TEXT . '</span>': ''); ?></label>
				<div class="clear-both"> </div>
				<?php echo zen_get_country_list('zone_country_id', $selected_country, 'id="country" ' . ($flag_show_pulldown_states == true ? 'onchange="xcheckout_update_zone(this.form);"' : '')); ?>
			</div>
			<div class="clear-both"> </div> <!-- COUNTRY -->
			
			<?php
			if (ACCOUNT_STATE == 'true') {
				if ($flag_show_pulldown_states == true) {
			?>
			<div class="field_account mini-input back">
				<label class="inputLabel" for="stateZone" id="zoneLabel"><?php echo ENTRY_STATE; if (zen_not_null(ENTRY_STATE_TEXT)) echo '&nbsp;<span class="x-alert">' . ENTRY_STATE_TEXT . '</span>'; ?></label>
				<div class="clear-both"> </div>
				<?php
					  echo zen_draw_pull_down_menu('zone_id', zen_prepare_country_zones_pull_down($selected_country), $zone_id, 'id="stateZone"');						  
				?>
			</div>
			<?php } ?> <!--STATE zone-->
			
			<div class="clearfix"></div>
			
			<div class="field_account mini-input back">
				<label class="inputLabel" for="state" id="stateLabel"><?php echo ($flag_show_pulldown_states) ? '' : ri('State', null, 'riCheckout');if (zen_not_null(ENTRY_STATE_TEXT)) echo '&nbsp;<span class="x-alert" id="stText">' . ENTRY_STATE_TEXT . '</span>';?></label>
				<div class="clear-both"> </div>
			
				<?php
					echo zen_draw_input_field('state', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_state', '40') . ' id="state"');				
					if ($flag_show_pulldown_states == false) {
					  echo zen_draw_hidden_field('zone_id', $zone_name, ' ');
					}
				?>
			</div>
			
			<div class="field_account mini-input">
				<label class="inputLabel" for="postcode"><?php echo ENTRY_POST_CODE . (zen_not_null(ENTRY_POST_CODE_TEXT) ? '<span class="x-alert">' . ENTRY_POST_CODE_TEXT . '</span>': ''); ?></label>
				<div class="clear-both"> </div>
				<?php echo zen_draw_input_field('postcode', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_postcode', '40') . ' id="postcode"'); ?>
			</div>
			<div class="clear-both"> </div> <!-- POST/ZIP -->
				
			<div class="field_account back">
				<label class="inputLabel" for="city"><?php echo ENTRY_CITY . (zen_not_null(ENTRY_CITY_TEXT) ? '<span class="x-alert">' . ENTRY_CITY_TEXT . '</span>': ''); ?></label>
				<div class="clear-both"> </div>
				<?php echo zen_draw_input_field('city', '', zen_set_field_length(TABLE_ADDRESS_BOOK, 'entry_city', '40') . ' id="city"'); ?>
			</div> <!-- CITY -->				 		

			
			
		</div> <!--eof xcheckout-right -->
		
		
	<?php
	}
	?>

		
	<!--  use -->
	<?php /*?>
	<div class="set-xpress back">
		<?php echo zen_draw_checkbox_field('shipping_set_xpress', 1, false, 'id="shipping_set_xpress"') ?>
		<label class="inputLabel2" for="shipping_set_xpress"><?php echo "Use this as your Express shipping address"; ?>
			(<a class="tooltip" title="<?php echo TEXT_EXPRESS_SHIPPING_HELP;?>"href="#">?</a>)
		</label>
	</div>
	<?php */?>
		<div class="clear-both"></div>
	<div class="buttonRow back shipping_billing" >
		<?php echo zen_draw_checkbox_field('same_shipping_payment_address', 1, false, 'id="same_shipping_payment_address" class="re_checkbox"') ?>
		<label class="inputLabel2 ajust" for="same_shipping_payment_address"><?php echo "Use Shipping address for Billing"; ?>
		</label>
	</div>
	<!-- end use -->
	
	</div> <!-- end div class group -->
	</fieldset>
	<?php if(zen_not_null(ENTRY_TELEPHONE_MIN_LENGTH) || ACCOUNT_FAX_NUMBER == 'true') { ?>
			<fieldset>
			<!--<legend><?php rie('ADDITIONAL CONTACT DETAILS'); ?></legend> -->
			<div class="group">
				<?php if(zen_not_null(ENTRY_TELEPHONE_MIN_LENGTH)) { ?>
					<div class="field_account">
					<label class="inputLabel" for="telephone"><?php echo ENTRY_TELEPHONE_NUMBER . (zen_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '<span class="x-alert">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>': ''); ?></label>
					<div class="clear-both"> </div>
					<?php echo zen_draw_input_field('telephone', '', zen_set_field_length(TABLE_CUSTOMERS, 'customers_telephone', '40') . ' id="telephone"'); ?>
					</div>
					<div class="clear-both"> </div>
				<?php 
				}
				?>
				
				<?php
				  if (ACCOUNT_FAX_NUMBER == 'true') {
				?>
					<div class="field_account">
					<label class="inputLabel" for="fax"><?php echo ENTRY_FAX_NUMBER . (zen_not_null(ENTRY_FAX_NUMBER_TEXT) ? '<span class="x-alert">' . ENTRY_FAX_NUMBER_TEXT . '</span>': ''); ?></label>
					<div class="clear-both"> </div>
					<?php echo zen_draw_input_field('fax', '', 'id="fax"'); ?>
					</div>
					<div class="clear-both"> </div>
				<?php
				  }
				?>
			</div>
			</fieldset>
		<?php 
		}
		?>

		<?php
		  if (ACCOUNT_DOB == 'true') {
		?>
			<fieldset>
			<legend><?php rie('ADDITIONAL PERSONAL DETAILS'); ?></legend>
			<div class="group">
				<div class="field_account">
				<label class="inputLabel" for="dob"><?php echo ENTRY_DATE_OF_BIRTH . (zen_not_null(ENTRY_DATE_OF_BIRTH_TEXT) ? '<span class="alert2">' . ENTRY_DATE_OF_BIRTH_TEXT . '</span>': ''); ?></label>
				<div class="clear-both"> </div>
				<?php echo zen_draw_input_field('dob','', 'id="dob"'); ?>
				</div>
				<div class="clear-both"> </div>
			</div>
			</fieldset>
		<?php
		  }
		?>

		<fieldset style="display:none">
		<legend><?php echo TABLE_HEADING_LOGIN_DETAILS; ?></legend>
		<div class="group">
			<div class="field_account">
			<label class="inputLabel" for="email-address"><?php echo ENTRY_EMAIL_ADDRESS . (zen_not_null(ENTRY_EMAIL_ADDRESS_TEXT) ? '<span class="x-alert">' . ENTRY_EMAIL_ADDRESS_TEXT . '</span>': ''); ?></label>
			<?php echo zen_draw_input_field('email_address',$_POST['login_email_address'], zen_set_field_length(TABLE_CUSTOMERS, 'customers_email_address', '40') . ' id="email-address"'); ?>
			<br class="clear-both" />
			</div>

			<?php
			  if ($phpBB->phpBB['installed'] == true) {
			?>
			<div class="field_account">
			<label class="inputLabel" for="nickname"><?php echo ENTRY_NICK . (zen_not_null(ENTRY_NICK_TEXT) ? '<span class="x-alert">' . ENTRY_NICK_TEXT . '</span>': ''); ?></label>
			<?php echo zen_draw_input_field('nick','','id="nickname"'); ?>
			<br class="clear-both" />
			</div>
			<?php
			  }
			?>

			<div class="field_account">
			<label class="inputLabel" for="password-new"><?php echo ENTRY_PASSWORD . (zen_not_null(ENTRY_PASSWORD_TEXT) ? '<span class="x-alert">' . ENTRY_PASSWORD_TEXT . '</span>': ''); ?></label>
			<?php echo zen_draw_password_field('password', $_POST['password_address'], zen_set_field_length(TABLE_CUSTOMERS, 'customers_password', '20') . ' id="password-new"'); ?>
			<br class="clear-both" />
			</div>

			<div class="field_account">
			<label class="inputLabel" for="password-confirm"><?php echo ENTRY_PASSWORD_CONFIRMATION . (zen_not_null(ENTRY_PASSWORD_CONFIRMATION_TEXT) ? '<span class="x-alert">' . ENTRY_PASSWORD_CONFIRMATION_TEXT . '</span>': ''); ?></label>
			<?php echo zen_draw_password_field('confirmation', $_POST['confirm_password'], zen_set_field_length(TABLE_CUSTOMERS, 'customers_password', '20') . ' id="password-confirm"'); ?>
			<br class="clear-both" />
			</div>
			<div class="clear-both"> </div>
		</div>
		</fieldset>
		<?php if(ACCOUNT_NEWSLETTER_STATUS != 0 || CUSTOMERS_REFERRAL_STATUS == 2){?>
		<fieldset>
		<legend><?php rie('NEWSLETTER'); ?></legend>
		<div class="group">
			<?php
			  if (ACCOUNT_NEWSLETTER_STATUS != 0) {
			?>
				<?php echo zen_draw_checkbox_field('newsletter', '1', $newsletter, 'id="newsletter-checkbox"') . '<label class="checkboxLabel" for="newsletter-checkbox">' . ENTRY_NEWSLETTER . '</label>' . (zen_not_null(ENTRY_NEWSLETTER_TEXT) ? '<span class="x-alert">' . ENTRY_NEWSLETTER_TEXT . '</span>': ''); ?>
				<div class="clear-both"> </div>
			<?php ?>
				<div class="field_account">
					<?php echo zen_draw_radio_field('email_format', 'HTML', ($email_format == 'HTML' ? true : false),'id="email-format-html"') . '<label class="radioButtonLabel" for="email-format-html">' . ENTRY_EMAIL_HTML_DISPLAY . '</label>' .  zen_draw_radio_field('email_format', 'TEXT', ($email_format == 'TEXT' ? true : false), 'id="email-format-text"') . '<label class="radioButtonLabel" for="email-format-text">' . ENTRY_EMAIL_TEXT_DISPLAY . '</label>'; ?>
				</div>
				<div class="clear-both"> </div>
			
			<?php
			} 
		  if (CUSTOMERS_REFERRAL_STATUS == 2) {
			?>
			<div class="group">
				<div class="field_account">
				<label class="inputLabel" for="customers_referral"><?php echo ENTRY_CUSTOMERS_REFERRAL; ?></label>
				<div class="clear-both"> </div>
				<?php echo zen_draw_input_field('customers_referral', '', zen_set_field_length(TABLE_CUSTOMERS, 'customers_referral', '15') . ' id="customers_referral"'); ?>
				</div>
				<div class="clear-both"> </div>

			</div>
			<?php } ?>
		</div>
		</fieldset>
		<?php } ?>
</div> <!-- end div id registry -->
<div class="clear-both"> </div>