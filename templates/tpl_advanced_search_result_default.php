<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=advanced_search_result.<br />
 * Displays results of advanced search
 *
 * @package templateSystem
 * @copyright Copyright 2003-2005 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_advanced_search_result_default.php 4182 2006-08-21 02:11:37Z ajeh $
 */
?>
<div class="centerColumn wrapper-978" id="advSearchResultsDefault">
<?php
//get product by categories
$record_per_page = (isset($_GET['record_per_page']) && (int)$_GET['record_per_page'] > 0) ? (int)$_GET['record_per_page'] : plugins\riPlugin\Plugin::get('settings')->get('theme.listing.record_per_page');

echo $riview->render('riElement::frontend/product_listing/style1/_products.php', array(
        'title' => $breadcrumb->last(),
        'current_route' => FILENAME_ADVANCED_SEARCH_RESULT,
        'listing_sql' => $listing_sql,
        'record_per_page' => $record_per_page,
        'column_list' => $column_list)
);
?>
</div>