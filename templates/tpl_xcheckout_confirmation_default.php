<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=checkout_confirmation.<br />
 * Displays final checkout details, cart, payment and shipping info details.
 *
 * @package templateSystem
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_checkout_confirmation_default.php 6247 2007-04-21 21:34:47Z wilt $
 */
?>
<?php if(\plugins\riPlugin\Plugin::get('riCheckout.Ajax')->startBlock('xcheckout_step')){?>
<div id="checkoutConfirmDefault">

<?php //if ($messageStack->size('redemptions') > 0) echo $messageStack->output('redemptions'); ?>
<?php //if ($messageStack->size('checkout_confirmation') > 0) echo $messageStack->output('checkout_confirmation'); ?>
<?php //if ($messageStack->size('checkout') > 0) echo $messageStack->output('checkout'); ?>

<h2 class="title_header cart"><?php rie('Products'); ?></h2>

<div class="buttonRow xcheckout-right">
    <a href="<?php echo zen_href_link(FILENAME_SHOPPING_CART, '', 'SSL')?>" class="not_ajax">
        <button class="btn btn-small"><?php rie('edit');?></button>
    </a>
</div>
<br class="clear-both" />

<?php  if ($flagAnyOutOfStock) { ?>
<?php    if (STOCK_ALLOW_CHECKOUT == 'true') {  ?>
<div class="messageStackError"><?php rie('Products marked with stock mark product out of stock are out of stock.<br />Items not in stock will be placed on backorder.', null, 'riCheckout'); ?></div>
<?php    } else { ?>
<div class="messageStackError"><?php rie('Products marked with stock mark product out of stock are out of stock or there are not enough in stock to fill your order.<br />Please change the quantity of products marked with stock mark product out of stock. Thank you', null, 'riCheckout'); ?></div>
<?php    } //endif STOCK_ALLOW_CHECKOUT ?>
<?php  } //endif flagAnyOutOfStock ?>


      <table border="0" cellspacing="0" cellpadding="0" id="cartContentsDisplay" class="table">
        <tr class="cartTableHeading">
        <th scope="col" id="ccImageHeading"></th>
        <th scope="col" id="ccProductsHeading"><?php echo TABLE_HEADING_PRODUCTS; ?></th>
       <!-- <th scope="col" id="ccDetailsHeading" width="150"><?php echo TABLE_HEADING_DETAILS; ?></th>-->
        <th scope="col" id="ccQuantityHeading"><?php echo TABLE_HEADING_QUANTITY; ?></th>
<?php
  // If there are tax groups, display the tax columns for price breakdown
  /*if (sizeof($order->info['tax_groups']) > 1) {
?>
          <th scope="col" id="ccTaxHeading" width="60"><?php echo HEADING_TAX; ?></th>
<?php
  }*/
?>
          <th scope="col" id="ccTotalHeading" ><?php echo TABLE_HEADING_TOTAL; ?></th>
        </tr>
<?php // now loop thru all products to display quantity and price ?>
<?php for ($i=0, $n=sizeof($order->products); $i<$n; $i++) { ?>
        <tr class="<?php echo $order->products[$i]['rowClass']; ?>">
          <td class="cartImage"><?php //echo zen_get_products_image((int)$order->products[$i]['id']);?></td>
         <td class="cartProductDisplay">
             <?php echo $order->products[$i]['name']; ?>
             <?php  echo $stock_check[$i]; ?>

             <?php // if there are attributes, loop thru them and display one per line
             if (isset($order->products[$i]['attributes']) && sizeof($order->products[$i]['attributes']) > 0 ) {
                 echo '<ul class="cartAttribsList">';
                 for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
                     ?>
                     <li><?php echo $order->products[$i]['attributes'][$j]['option'] . ': ' . nl2br(zen_output_string_protected($order->products[$i]['attributes'][$j]['value'])); ?></li>
                     <?php
                 } // end loop
                 echo '</ul>';
             } // endif attribute-info
             ?>
         </td>
         <!--  <td class="cartDetailDisplay">
          <?php // if there are attributes, loop thru them and display one per line
          if (isset($order->products[$i]['attributes']) && sizeof($order->products[$i]['attributes']) > 0 ) {
            echo '<ul class="cartAttribsList">';
            for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
            ?>
        		<li><?php echo $order->products[$i]['attributes'][$j]['option'] . ': ' . nl2br(zen_output_string_protected($order->products[$i]['attributes'][$j]['value'])); ?></li>
            <?php
            } // end loop
            echo '</ul>';
            } // endif attribute-info
          ?>
          </td>-->
          <td class="cartQuantity"><?php echo $order->products[$i]['qty']; ?>&nbsp;x <br />
          <?php  echo $stock_check[$i]; ?>
		  </td>


<?php // display tax info if exists ?>
<?php /*if (sizeof($order->info['tax_groups']) > 1)  { ?>
        <td class="cartTotalDisplay">
          <?php echo zen_display_tax_value($order->products[$i]['tax']); ?>%</td>
<?php    }  */ // endif tax info display  ?>
        <td class="cartTotalDisplay">
          <?php echo $currencies->display_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']);
          if ($order->products[$i]['onetime_charges'] != 0 ) echo '<br /> ' . $currencies->display_price($order->products[$i]['onetime_charges'], $order->products[$i]['tax'], 1);
?>
        </td>
      </tr>
<?php  }  // end for loopthru all products ?>
	<tr>
		<td colspan="2"></td>
      <td id='total-groups' colspan="<?php echo sizeof($order->info['tax_groups']) > 1 ? 4 : 3 ?>">
      <?php
      if (MODULE_ORDER_TOTAL_INSTALLED) {
      $order_totals = $order_total_modules->process();
      ?>
      <div id="orderTotals"><?php $order_total_modules->output(); ?></div>
      <?php
       }
?>

      </td></tr>
      </table>

<?php
	if(strpos($form_action_url, zen_href_link(FILENAME_XCHECKOUT_CONFIRMATION)) === false	&& strpos($form_action_url, zen_href_link(FILENAME_CHECKOUT_PROCESS, '', 'SSL')) === false)
		$css_class = ' class="not_ajax"';
		
  echo zen_draw_form('checkout_confirmation', $form_action_url, 'post', 'id="checkout_confirmation"'.$css_class);

  if (is_array($payment_modules->modules)) {
    echo $payment_modules->process_button();
  }
?>

    <span class="loader" style="display: none;">
    <?php rie('Loading');?>
    </span>
    <button name='btn_submit' class='xcheckout-right btn btn-success btn-large'><?php rie('Confirm');?></button>
    <div class="clear-both"></div>
</form>
</div>
<?php \plugins\riPlugin\Plugin::get('riCheckout.Ajax')->endBlock();}
require($template->get_template_dir('tpl_modules_xcheckout_tracking.php', DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout'). '/' . 'tpl_modules_xcheckout_tracking.php');