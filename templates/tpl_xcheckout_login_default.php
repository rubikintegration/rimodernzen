
<?php if(\plugins\riPlugin\Plugin::get('riCheckout.Ajax')->startBlock('xcheckout_step')){?>
<div id="loginFormWrapper" class="forward" style="width:100%">
				
				<?php echo zen_draw_form('loginForm', zen_href_link(FILENAME_XLOGIN, 'action=process', 'SSL'), 'post'); ?>


					<ul id="checkout-login-selection" class="font-droidsansbold">
						<li><input name="choose" data-target=".login" data-action="<?php echo zen_href_link(FILENAME_XLOGIN,'action=process', 'SSL')?>" type="radio" value="1" checked="checked"> <?php rie('Returning Customer');?></li>
						<li><input name="choose" data-target=".register" data-action="<?php echo zen_href_link(FILENAME_XCREATE_ACCOUNT, '', 'SSL')?>" type="radio" value="2"> <?php rie('New Customer, I want to Register');?></li>
						<!--<li><input name="choose" data-target=".noregister" data-action="<?php echo zen_href_link(FILENAME_XNO_ACCOUNT, '', 'SSL')?>" type="radio" value="3"> <?php rie('New Customer, I do not want to register');?></li>-->
					</ul>
					<div id="checkout-login-form">
						<div class="section login register noregister">
                            <label><?php rie('Email Address:');?></label>
                            <input class="account" type="text" name="login_email_address" />
                            <div class="clear-both"></div>
                        </div>

						<div class="login section">
                            <label><?php rie('Password:');?></label>
                            <input class="account" type="password" name="login_password" />
                            <a class="standard" href="<?php echo zen_href_link(FILENAME_PASSWORD_FORGOTTEN);?>"><?php rie('Forgot Your Password?');?></a>
                            <div class="clear-both"></div>
                        </div>

						<div class="register section" style="display:none">
                            <label><?php rie('Select Password:');?></label>
                            <input class="account" type="password" name="password_address" />
                            <div class="clear-both"></div>
                        </div>

						<div class="register section" style="display:none">
                            <label><?php rie('Confirm Password:');?></label>
                            <input class="account" type="password" name="confirm_password" />
                            <div class="clear-both"></div>
                        </div>

						<div class="section noregister" style="display:none">
                            <label><?php rie('Confirm Email:');?></label>
                            <input class="account" type="text" name="confirm_email" />
                            <div class="clear-both"></div>
                        </div>

						<div class="clear-both"></div>
					</div>
                    <button class="xcheckout-next-button btn btn-primary btn-x right"><?php rie('CONTINUE')?></button>

					<div class="clear-both"></div>
					<?php echo zen_draw_hidden_field('javascript_enabled', 1);?>
					<?php echo zen_draw_hidden_field('xcheckout', 1);?>
					<?php echo zen_draw_hidden_field('securityToken', $_SESSION['securityToken']); ?>

				</form>
				
				</div>
				<div class="clear-both"></div>
				<span class="loader" style="display: none;">
				<?php rie('Loading');?>
                </span>
<script type="text/javascript">
    document.loginForm.login_email_address.focus();
</script>

<?php \plugins\riPlugin\Plugin::get('riCheckout.Ajax')->endBlock();}

require($template->get_template_dir('tpl_modules_xcheckout_tracking.php', DIR_WS_TEMPLATE, $current_page_base,'templates/xcheckout'). '/' . 'tpl_modules_xcheckout_tracking.php');