<?php global $request_type; ?>
<script languague="javascript">
jQuery(document).ready(function(){
    
   //add to cart ajax
   if(jQuery('form[name=cart_quantity]').is('*')){
        jQuery('form[name=cart_quantity]').submit(function(){
            jQuery.ajax({
                url: "<?php echo riLink('ricart_ajax_add_to_cart', array(), $request_type, false, 'ri.php');?>",
                dataType: 'json',
                type: 'post',
                data: jQuery(this).serialize(),
                success: function(res){
                    jQuery(document).trigger('onCartUpdate', res);
                }
            });
            return false;
        });			
    }     

   ///////////////////////////////////////////////////////////

    jQuery('.img-small-product-info').on('click', function(){
        jQuery('.img-small-product-info').css('border', '1px solid #c1c1c1');
        var src = jQuery(this).attr('src');
        jQuery('.img-product-info').attr('src',src);
        jQuery('#zoom-img a').attr('href', src);
        jQuery(this).css('border', '1px solid red');
    });
});
</script>