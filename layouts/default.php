<?php
/**
 * Created by RubikIntegration Team.
 * Date: 9/2/12
 * Time: 10:14 AM
 * Question? Come to our website at http://rubikintegration.com
 */
?>
<!DOCTYPE html>
<html <?php echo HTML_PARAMS; ?>>
<head>
    <?php
    /**
    * load the module for generating page meta-tags
    */
    require(DIR_WS_MODULES . zen_get_module_directory('meta_tags.php'));
    /**
    * output main page HEAD tag and related headers/meta-tags, etc
    */
    ?>
    <?php echo $riview->render('riElement::frontend/head/_head.php');?>
    <?php $riview->get('loader')->load(array('jquery.lib', 'bootstrap.lib', 'modern_zen.css'));?>
</head>
<?php
// the main body

$body_id = ($this_is_home_page) ? 'indexHome' : str_replace('_', '', $_GET['main_page']);
?>
<body id="<?php echo $body_id . 'Body'; ?>"<?php if($zv_onload !='') echo ' onload="'.$zv_onload.'"'; ?>>
<!-- holder: abc -->
<div id="ri-container">
    <div id="header-wrapper">
        <!-- holder: globalBox1 -->
    </div>
    <div class="clear-both"></div>
    <!--////////////////////////////////////////////////////////////////////////-->

    <div id="main-wrapper" class="wrapper-978">
        <div id="breadcrumbs">Home >></div>
        <?php
        /**
         * prepares and displays center column
         *
         */
        require($body_code); ?>
    </div>
    <div class="clear-footer"></div>
</div><!--End Container-->
<div id="ri-footer">
    <!-- holder: globalBox3 -->
</div>
<?php $riview->get('loader')->load(array('my-script.php'=> array('type' => 'js')));?>
<!--bof- parse time display -->
<?php
if (DISPLAY_PAGE_PARSE_TIME == 'true') {
    ?>
<div class="smallText center">Parse Time: <?php echo $parse_time; ?> - Number of Queries: <?php echo $db->queryCount(); ?> - Query Time: <?php echo $db->queryTime(); ?></div>
    <?php
}
?>
<!--eof- parse time display -->

</body>
</html>